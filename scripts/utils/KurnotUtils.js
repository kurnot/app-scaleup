const fs = require('fs-extra');
const path = require('path');

const getCommonsRoute = function(){
    console.log("Obtener la ruta donde va el commons");

    var kurnotDependenciesFile = path.join('kurnot.dependencies.json');

    if(fs.existsSync(kurnotDependenciesFile)){
        var kurnotDependencies = JSON.parse(fs.readFileSync(kurnotDependenciesFile));

        if(Object.keys(kurnotDependencies).indexOf("@kurnot-commons") != -1){
            let commonsRoute = kurnotDependencies["@kurnot-commons"];

            return path.join(__dirname, '../..', commonsRoute);
        }
    }

    return null;
}

module.exports = {
    getCommonsRoute
}