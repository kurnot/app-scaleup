const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const { exit } = require('process');
const rimraf = require('rimraf');
const tmp = os.tmpdir();
const shell = require('shelljs');
const { getCommonsRoute } = require('./utils/KurnotUtils');

const commonsRoute = getCommonsRoute();

console.log("--------------------------------------");
console.log("            BUILD COMMONS ");
console.log("--------------------------------------");

shell.cd(commonsRoute);
console.log(commonsRoute)
shell.exec("yarn build");

console.log("--------------------------------------");
console.log("            BUILD APP ");
console.log("--------------------------------------");
const appPath = path.join(__dirname, '..');
shell.cd(appPath);
shell.exec("yarn build");

console.log("--------------------------------------");
console.log("            DEPLOY APP ");
console.log("--------------------------------------");
shell.cd(path.join(appPath, "firebase"));
shell.exec("npm run deploy");