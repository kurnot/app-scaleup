const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const { exit } = require('process');
const rimraf = require('rimraf');
const tmp = os.tmpdir();
const shell = require('shelljs');
const Bitbucket = require('bitbucket').Bitbucket;

// 1. If exists, read kurnot customs file
var kurnotDependenciesFile = path.join('kurnot.dependencies.json');
var kurnotCredentialsFile = path.join('./.kurnot/credentials.json');
var credentials = {};

if(fs.existsSync(kurnotCredentialsFile)){
    credentials = JSON.parse(fs.readFileSync(kurnotCredentialsFile));
}else{
    console.error("You must be define Bitbucket credentials at .kurnot/credentials.json with format {username : 'xxxx', password : 'xxxx'}");
    exit(0);
}

const bitbucket = new Bitbucket({
    auth: credentials
});

const kurnotCommonsTmpDir = path.join(tmp, `kurnot-commons-tmp-`);
let temporalDirectories = [];

const getDependencies = async function(){
    if(fs.existsSync(kurnotDependenciesFile)){
        var kurnotDependencies = JSON.parse(fs.readFileSync(kurnotDependenciesFile));
    
        var dependenciesKeys = Object.keys(kurnotDependencies);

        
        for(var i = 0; i < dependenciesKeys.length; i++){
            var dependencyKey = dependenciesKeys[i];
            var dependency = kurnotDependencies[dependencyKey];
            var dependencyDir = path.join(__dirname, "../packages");

            console.log("Dependency dir ", dependencyDir);
            
            if(fs.existsSync(dependencyDir)){
                rimraf.sync(dependencyDir);
            }

            // If folder not exists, create route
            var dependencyRoute = path.join(__dirname, '..', dependency, "./packages");

            console.log("Dependency rute",dependencyDir, dependency);

            //console.log("Link route", dependencyModule,linkRoute);
            if(!dependency.startsWith('./') && !dependency.startsWith('../')){
                dependencyRoute = kurnotCommonsTmpDir + dependency;
                
                if(!fs.existsSync(dependencyRoute)){
                    const tagInfo = await bitbucket.repositories.getTag({
                        workspace : 'Kurnot',
                        repo_slug : 'kurnot-commons',
                        name : dependency
                    });

                    shell.cd(tmp);
                    let link = tagInfo.data.links.html.href.split("https://");
                    link = `https://${credentials.username}:${credentials.password}@${link[1]}`;
                    
                    shell.exec(`git clone ${link} --branch ${dependency} kurnot-commons-tmp-${dependency}`);

                    temporalDirectories.push(dependencyRoute);

                    shell.cd(dependencyRoute);
                    shell.exec("yarn install");
                }

                // Recalcular dependency route
                let dependencyModule = dependencyKey.split("/")[1];
                let dependencyDist = path.join(kurnotCommonsTmpDir + dependency, `packages/${dependencyModule}`);
                /* if(!fs.existsSync(path.join(dependencyDist, 'index.js'))){
                    dependencyDist = path.join(dependencyDist, dependencyModule);
                } */

                dependencyRoute = dependencyDist;
            }

            console.log(`Linking route ${dependencyDir} -> ${dependencyRoute}`);
            if(fs.existsSync(dependencyDir)){
                rimraf.sync(dependencyDir);
            }
            
            fs.symlinkSync(dependencyRoute, dependencyDir, 'dir');

        }

        /* for(let j = 0; j < temporalDirectories.length; j++){
            let temporalDirectory = temporalDirectories[j];
            rimraf.sync(temporalDirectory);
        } */
    }
}

getDependencies();