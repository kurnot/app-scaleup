import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import LinkingConfiguration from "./config/routes";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from "./screens/login/Login";
import Asset from "./screens/asset/Asset";
import { Home } from "./screens/home";
import { Activities } from "screens/activities";
import { Users } from "screens/users";
import { Text } from "react-native";
import { configureAppStore, ThemeContext, saveItem } from "@kurnot-commons/app-core";
import { reducers } from "./config/redux/reducers";
import { Provider } from "react-redux";
import { initializeApp } from 'firebase/app';
import { getAnalytics, logEvent } from "firebase/analytics";
import { getConfig } from "./config/ConfigUtils";
import palette from "./config/palette";
import { Screen, KurnotProvider } from "@kurnot-commons/components";
import { ROLES } from "config/Roles";
import Manage from "./screens/manage/Manage";

const ROUTES = [
  {
    name: "Home",
    component: Home,
    options: { headerShown: false, showHeader : false },
    url: "",
    title: "Inicio",
  },
  {
    name: "Login",
    component: Login,
    options: { headerShown: false, showHeader : false },
    url: "login",
    title: "Iniciar sesión",
    header: false,
  },
  {
    name: "Activities",
    component: Activities,
    options: { headerShown: false, showHeader : false },
    url: "activities",
    title: "Actividades",
    header: false,
  },
  {
    name: "Students",
    component: Users,
    options: { headerShown: false, showHeader : false },
    url: "students",
    title: "Colegiales",
    header: false,
  },
  {
    name: "Asset",
    component: Asset,
    options: { headerShown: false },
    url: "asset",
    title: "XTZ",
    authenticated: true,
		restrictedTo : [ROLES.ADMIN]
  },
  {
    name : "Manage",
    component: Manage,
    options: { headerShown: false },
    url: "manage",
    title: "Gestionar ScaleUp",
    authenticated: true,
		restrictedTo : [ROLES.ADMIN]
  }
];

const WebApp = (propsParam : any) => {
  const props = propsParam.parentProps;
  const store = propsParam.store;

  const [page, setPage] = useState(null);
    let pageUrl = props.query.page ? props.query.page[0] : "";
    let Page: any = ROUTES.filter((ROUTE) => {
      // TODO Esto bien y con regex y fallback
      if (page) {
        return ROUTE.name == page;
      }

      return ROUTE.url == pageUrl;
    });

    if (typeof window != "undefined") {
      window.history.pushState({}, Page[0].title, Page[0].url);
    }

    const Component = Page.length > 0 ? Page[0].component : () => <Text>404</Text>;
    Page = Page.length > 0 ? Page[0] : null;

    const navigate = (componentName: any) => {
      setPage(componentName);

      // TODO hacer todo esto bien, guardar un titulo tambien y poner params si hay tipo path, query o lo que sea
      const ROUTE = ROUTES.filter(
        (ROUTE) => ROUTE.name == componentName
      )[0];

      window.history.pushState({}, ROUTE.title, ROUTE.url);
    };

    if(typeof window != 'undefined'){
      // @ts-ignore
      window['navigate'] = navigate;
    }

    return (
      <Provider store={store}>
        <KurnotProvider palette={palette} store={store} config={propsParam.config}>
          <Screen
              component={Component}
              navigation={{
                navigate
              }}
              {...props}
              {...Page}
            ></Screen>
        </KurnotProvider>
      </Provider> 
    );
}

export default function App(props: any) {
  const Stack = createNativeStackNavigator();
  const config = getConfig();

  let webAnalytics = null;
  if (typeof window !== 'undefined') {
    const app = initializeApp(config.firebase);
    webAnalytics = getAnalytics(app);
  }

  console.log("Entro a reconfigurar el store");
  const store = configureAppStore({}, null, reducers, webAnalytics);
  
  if (props.query) {
    return <WebApp parentProps={props} store={store} config={config}/>
  }

  return (
    <ThemeContext.Provider value={palette}>
      <Provider store={store}>
        <NavigationContainer linking={LinkingConfiguration}>
          <Stack.Navigator initialRouteName="Asset">
            {ROUTES.map((ROUTE: any, i: number) => (
              <Stack.Screen key={i} name={ROUTE.name} options={ROUTE.options}>
                {(props) => (
                  <Screen
                    component={ROUTE.component}
                    {...props}
                    {...ROUTE}
                  ></Screen>
                )}
              </Stack.Screen>
            ))}
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    </ThemeContext.Provider>
  );
}

