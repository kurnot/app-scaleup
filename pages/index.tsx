// @generated: @expo/next-adapter@3.1.17
import React from 'react';
import App from '../App';
import { useRouter } from 'next/router';

function Index() {

  const router = useRouter();

  return (
    <App query={router.query}/>
  );
}

Index.getInitialProps = async () => {
  return {}
}

export default Index;