// @generated: @expo/next-adapter@3.1.17
import React from 'react';
import App from '../App';
import { useRouter } from 'next/router';

function Index() {

  const router = useRouter();

  if(router.query && router.query.page && router.query.page.indexOf('public') != -1){
    console.log("La query es", router.query);
    console.log("A devolver un estatico");
  }

  return (
    <App query={router.query}/>
  );
}

Index.getInitialProps = async () => {
  return {}
}

export default Index;