module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+es4":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/transition");

/***/ }),

/***/ "+mtM":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var normalize_css_color__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("DjSU");
/* harmony import */ var normalize_css_color__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(normalize_css_color__WEBPACK_IMPORTED_MODULE_0__);
var processColor=function processColor(color){if(color===undefined||color===null){return color;}var int32Color=normalize_css_color__WEBPACK_IMPORTED_MODULE_0___default()(color);if(int32Color===undefined||int32Color===null){return undefined;}int32Color=(int32Color<<24|int32Color>>>8)>>>0;return int32Color;};/* harmony default export */ __webpack_exports__["a"] = (processColor);

/***/ }),

/***/ "/GRZ":
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),

/***/ "0G5g":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
exports.__esModule=true;exports.cancelIdleCallback=exports.requestIdleCallback=void 0;var requestIdleCallback=typeof self!=='undefined'&&self.requestIdleCallback||function(cb){var start=Date.now();return setTimeout(function(){cb({didTimeout:false,timeRemaining:function timeRemaining(){return Math.max(0,50-(Date.now()-start));}});},1);};exports.requestIdleCallback=requestIdleCallback;var cancelIdleCallback=typeof self!=='undefined'&&self.cancelIdleCallback||function(id){return clearTimeout(id);};exports.cancelIdleCallback=cancelIdleCallback;

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("5w0S");


/***/ }),

/***/ "1ccW":
/***/ (function(module, exports) {

function _extends() {
  module.exports = _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "2+G7":
/***/ (function(module, exports) {

module.exports = require("styled-jsx/server");

/***/ }),

/***/ "24z6":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var _slicedToArray=__webpack_require__("zoAU");var _interopRequireDefault=__webpack_require__("AroE");exports.__esModule=true;exports.initScriptLoader=initScriptLoader;exports.default=void 0;var _extends2=_interopRequireDefault(__webpack_require__("1ccW"));var _objectWithoutPropertiesLoose2=_interopRequireDefault(__webpack_require__("98FW"));var _react=__webpack_require__("cDcd");var _headManagerContext=__webpack_require__("Y8yu");var _headManager=__webpack_require__("DqTX");var _requestIdleCallback=__webpack_require__("0G5g");var ScriptCache=new Map();var LoadCache=new Set();var ignoreProps=['onLoad','dangerouslySetInnerHTML','children','onError','strategy'];var loadScript=function loadScript(props){var src=props.src,id=props.id,_props$onLoad=props.onLoad,onLoad=_props$onLoad===void 0?function(){}:_props$onLoad,dangerouslySetInnerHTML=props.dangerouslySetInnerHTML,_props$children=props.children,children=_props$children===void 0?'':_props$children,onError=props.onError;var cacheKey=id||src;if(ScriptCache.has(src)){if(!LoadCache.has(cacheKey)){LoadCache.add(cacheKey);ScriptCache.get(src).then(onLoad,onError);}return;}var el=document.createElement('script');var loadPromise=new Promise(function(resolve,reject){el.addEventListener('load',function(){resolve();if(onLoad){onLoad.call(this);}});el.addEventListener('error',function(){reject();if(onError){onError();}});});if(src){ScriptCache.set(src,loadPromise);LoadCache.add(cacheKey);}if(dangerouslySetInnerHTML){el.innerHTML=dangerouslySetInnerHTML.__html||'';}else if(children){el.textContent=typeof children==='string'?children:Array.isArray(children)?children.join(''):'';}else if(src){el.src=src;}for(var _i=0,_Object$entries=Object.entries(props);_i<_Object$entries.length;_i++){var _ref=_Object$entries[_i];var _ref2=_slicedToArray(_ref,2);var k=_ref2[0];var value=_ref2[1];if(value===undefined||ignoreProps.includes(k)){continue;}var attr=_headManager.DOMAttributeNames[k]||k.toLowerCase();el.setAttribute(attr,value);}document.body.appendChild(el);};function handleClientScriptLoad(props){var _props$strategy=props.strategy,strategy=_props$strategy===void 0?'afterInteractive':_props$strategy;if(strategy==='afterInteractive'){loadScript(props);}else if(strategy==='lazyOnload'){window.addEventListener('load',function(){(0,_requestIdleCallback.requestIdleCallback)(function(){return loadScript(props);});});}}function loadLazyScript(props){if(document.readyState==='complete'){(0,_requestIdleCallback.requestIdleCallback)(function(){return loadScript(props);});}else{window.addEventListener('load',function(){(0,_requestIdleCallback.requestIdleCallback)(function(){return loadScript(props);});});}}function initScriptLoader(scriptLoaderItems){scriptLoaderItems.forEach(handleClientScriptLoad);}function Script(props){var _props$src=props.src,src=_props$src===void 0?'':_props$src,_props$onLoad2=props.onLoad,onLoad=_props$onLoad2===void 0?function(){}:_props$onLoad2,_props$strategy2=props.strategy,strategy=_props$strategy2===void 0?'afterInteractive':_props$strategy2,onError=props.onError,restProps=(0,_objectWithoutPropertiesLoose2.default)(props,["src","onLoad","dangerouslySetInnerHTML","strategy","onError"]);var _ref3=(0,_react.useContext)(_headManagerContext.HeadManagerContext),updateScripts=_ref3.updateScripts,scripts=_ref3.scripts;(0,_react.useEffect)(function(){if(strategy==='afterInteractive'){loadScript(props);}else if(strategy==='lazyOnload'){loadLazyScript(props);}},[props,strategy]);if(true){return null;}if(strategy==='beforeInteractive'){if(updateScripts){scripts.beforeInteractive=(scripts.beforeInteractive||[]).concat([(0,_extends2.default)({src:src,onLoad:onLoad,onError:onError},restProps)]);updateScripts(scripts);}}return null;}var _default=Script;exports.default=_default;

/***/ }),

/***/ "48fX":
/***/ (function(module, exports, __webpack_require__) {

var setPrototypeOf = __webpack_require__("qhzo");

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) setPrototypeOf(subClass, superClass);
}

module.exports = _inherits;

/***/ }),

/***/ "56YO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/modules/getBoundingClientRect/index.js
var getBoundingClientRect = __webpack_require__("Q/JM");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/modules/unitlessNumbers/index.js
var unitlessNumbers = __webpack_require__("ou6r");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/setValueForStyles/dangerousStyleValue.js
function dangerousStyleValue(name,value,isCustomProperty){var isEmpty=value==null||typeof value==='boolean'||value==='';if(isEmpty){return'';}if(!isCustomProperty&&typeof value==='number'&&value!==0&&!(unitlessNumbers["a" /* default */].hasOwnProperty(name)&&unitlessNumbers["a" /* default */][name])){return value+'px';}return(''+value).trim();}/* harmony default export */ var setValueForStyles_dangerousStyleValue = (dangerousStyleValue);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/setValueForStyles/index.js
function setValueForStyles(node,styles){var style=node.style;for(var styleName in styles){if(!styles.hasOwnProperty(styleName)){continue;}var isCustomProperty=styleName.indexOf('--')===0;var styleValue=setValueForStyles_dangerousStyleValue(styleName,styles[styleName],isCustomProperty);if(styleName==='float'){styleName='cssFloat';}if(isCustomProperty){style.setProperty(styleName,styleValue);}else{style[styleName]=styleValue;}}}/* harmony default export */ var modules_setValueForStyles = (setValueForStyles);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/UIManager/index.js
var UIManager_getRect=function getRect(node){var _getBoundingClientRec=Object(getBoundingClientRect["a" /* default */])(node),x=_getBoundingClientRec.x,y=_getBoundingClientRec.y,top=_getBoundingClientRec.top,left=_getBoundingClientRec.left;var width=node.offsetWidth;var height=node.offsetHeight;return{x:x,y:y,width:width,height:height,top:top,left:left};};var _measureLayout=function measureLayout(node,relativeToNativeNode,callback){var relativeNode=relativeToNativeNode||node&&node.parentNode;if(node&&relativeNode){setTimeout(function(){var relativeRect=Object(getBoundingClientRect["a" /* default */])(relativeNode);var _getRect=UIManager_getRect(node),height=_getRect.height,left=_getRect.left,top=_getRect.top,width=_getRect.width;var x=left-relativeRect.left;var y=top-relativeRect.top;callback(x,y,width,height,left,top);},0);}};var focusableElements={A:true,INPUT:true,SELECT:true,TEXTAREA:true};var UIManager={blur:function blur(node){try{node.blur();}catch(err){}},focus:function focus(node){try{var name=node.nodeName;if(node.getAttribute('tabIndex')==null&&focusableElements[name]==null){node.setAttribute('tabIndex','-1');}node.focus();}catch(err){}},measure:function measure(node,callback){_measureLayout(node,null,callback);},measureInWindow:function measureInWindow(node,callback){if(node){setTimeout(function(){var _getRect2=UIManager_getRect(node),height=_getRect2.height,left=_getRect2.left,top=_getRect2.top,width=_getRect2.width;callback(left,top,width,height);},0);}},measureLayout:function measureLayout(node,relativeToNativeNode,onFail,onSuccess){_measureLayout(node,relativeToNativeNode,onSuccess);},updateView:function updateView(node,props){for(var prop in props){if(!Object.prototype.hasOwnProperty.call(props,prop)){continue;}var value=props[prop];switch(prop){case'style':{modules_setValueForStyles(node,value);break;}case'class':case'className':{node.setAttribute('class',value);break;}case'text':case'value':node.value=value;break;default:node.setAttribute(prop,value);}}},configureNextLayoutAnimation:function configureNextLayoutAnimation(config,onAnimationDidEnd){onAnimationDidEnd();},setLayoutAnimationEnabledExperimental:function setLayoutAnimationEnabledExperimental(){}};/* harmony default export */ var exports_UIManager = __webpack_exports__["a"] = (UIManager);

/***/ }),

/***/ "5Bvt":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return pick; });
function pick(obj,list){var nextObj={};for(var key in obj){if(obj.hasOwnProperty(key)){if(list[key]===true||key.indexOf('aria-')===0){nextObj[key]=obj[key];}}}return nextObj;}

/***/ }),

/***/ "5fIB":
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__("7eYB");

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return arrayLikeToArray(arr);
}

module.exports = _arrayWithoutHoles;

/***/ }),

/***/ "5pvt":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/grid");

/***/ }),

/***/ "5w0S":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* reexport */ next_adapter_document; });

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("cpVT");

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/classCallCheck.js
var classCallCheck = __webpack_require__("H+61");

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/createClass.js
var createClass = __webpack_require__("UlJF");

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/inherits.js
var inherits = __webpack_require__("7LId");

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js + 2 modules
var possibleConstructorReturn = __webpack_require__("VIvw");

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js
var getPrototypeOf = __webpack_require__("iHvq");

// EXTERNAL MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js
var asyncToGenerator = __webpack_require__("rg98");

// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__("o0o1");
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);

// EXTERNAL MODULE: ./node_modules/next/document.js
var next_document = __webpack_require__("8cZr");
var document_default = /*#__PURE__*/__webpack_require__.n(next_document);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/AppRegistry/index.js + 2 modules
var AppRegistry = __webpack_require__("yKOc");

// CONCATENATED MODULE: ./node_modules/@expo/next-adapter/document.js
var __jsx=external_react_["createElement"];function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);enumerableOnly&&(symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;})),keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=null!=arguments[i]?arguments[i]:{};i%2?ownKeys(Object(source),!0).forEach(function(key){Object(defineProperty["a" /* default */])(target,key,source[key]);}):Object.getOwnPropertyDescriptors?Object.defineProperties(target,Object.getOwnPropertyDescriptors(source)):ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}return target;}function _createSuper(Derived){var hasNativeReflectConstruct=_isNativeReflectConstruct();return function _createSuperInternal(){var Super=Object(getPrototypeOf["a" /* default */])(Derived),result;if(hasNativeReflectConstruct){var NewTarget=Object(getPrototypeOf["a" /* default */])(this).constructor;result=Reflect.construct(Super,arguments,NewTarget);}else{result=Super.apply(this,arguments);}return Object(possibleConstructorReturn["a" /* default */])(this,result);};}function _isNativeReflectConstruct(){if(typeof Reflect==="undefined"||!Reflect.construct)return false;if(Reflect.construct.sham)return false;if(typeof Proxy==="function")return true;try{Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],function(){}));return true;}catch(e){return false;}}var style="\n/**\n * Building on the RNWeb reset:\n * https://github.com/necolas/react-native-web/blob/master/packages/react-native-web/src/exports/StyleSheet/initialRules.js\n */\nhtml, body, #__next {\n  width: 100%;\n  /* To smooth any scrolling behavior */\n  -webkit-overflow-scrolling: touch;\n  margin: 0px;\n  padding: 0px;\n  /* Allows content to fill the viewport and go beyond the bottom */\n  min-height: 100%;\n}\n#__next {\n  flex-shrink: 0;\n  flex-basis: auto;\n  flex-direction: column;\n  flex-grow: 1;\n  display: flex;\n  flex: 1;\n}\nhtml {\n  scroll-behavior: smooth;\n  /* Prevent text size change on orientation change https://gist.github.com/tfausak/2222823#file-ios-8-web-app-html-L138 */\n  -webkit-text-size-adjust: 100%;\n  height: 100%;\n}\nbody {\n  display: flex;\n  /* Allows you to scroll below the viewport; default value is visible */\n  overflow-y: auto;\n  overscroll-behavior-y: none;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  -ms-overflow-style: scrollbar;\n}\n";function getInitialProps(_x){return _getInitialProps.apply(this,arguments);}function _getInitialProps(){_getInitialProps=Object(asyncToGenerator["a" /* default */])(regenerator_default.a.mark(function _callee(_ref){var renderPage,_AppRegistry$getAppli,getStyleElement,page,styles;return regenerator_default.a.wrap(function _callee$(_context){while(1){switch(_context.prev=_context.next){case 0:renderPage=_ref.renderPage;AppRegistry["a" /* default */].registerComponent('Main',function(){return next_document["Main"];});_AppRegistry$getAppli=AppRegistry["a" /* default */].getApplication('Main'),getStyleElement=_AppRegistry$getAppli.getStyleElement;page=renderPage();styles=[external_react_["createElement"]("style",{dangerouslySetInnerHTML:{__html:style}}),getStyleElement()];return _context.abrupt("return",_objectSpread(_objectSpread({},page),{},{styles:external_react_["Children"].toArray(styles)}));case 6:case"end":return _context.stop();}}},_callee);}));return _getInitialProps.apply(this,arguments);}var document_Document=function(_NextDocument){Object(inherits["a" /* default */])(Document,_NextDocument);var _super=_createSuper(Document);function Document(){Object(classCallCheck["a" /* default */])(this,Document);return _super.apply(this,arguments);}Object(createClass["a" /* default */])(Document,[{key:"render",value:function render(){return external_react_["createElement"](next_document["Html"],null,external_react_["createElement"](next_document["Head"],null,external_react_["createElement"]("meta",{httpEquiv:"X-UA-Compatible",content:"IE=edge"})),external_react_["createElement"]("body",null,external_react_["createElement"](next_document["Main"],null),external_react_["createElement"](next_document["NextScript"],null)));}}]);return Document;}(document_default.a);document_Document.getInitialProps=getInitialProps;/* harmony default export */ var next_adapter_document = (document_Document);
// CONCATENATED MODULE: ./pages/_document.js


/***/ }),

/***/ "7H9r":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var CSS_UNIT_RE=/^[+-]?\d*(?:\.\d+)?(?:[Ee][+-]?\d+)?(%|\w*)/;var getUnit=function getUnit(str){return str.match(CSS_UNIT_RE)[1];};var isNumeric=function isNumeric(n){return!isNaN(parseFloat(n))&&isFinite(n);};var multiplyStyleLengthValue=function multiplyStyleLengthValue(value,multiple){if(typeof value==='string'){var number=parseFloat(value)*multiple;var unit=getUnit(value);return""+number+unit;}else if(isNumeric(value)){return value*multiple;}};/* harmony default export */ __webpack_exports__["a"] = (multiplyStyleLengthValue);

/***/ }),

/***/ "7LId":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _inherits; });
/* harmony import */ var _babel_runtime_helpers_esm_setPrototypeOf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("t4dY");

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object(_babel_runtime_helpers_esm_setPrototypeOf__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(subClass, superClass);
}

/***/ }),

/***/ "7eYB":
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

module.exports = _arrayLikeToArray;

/***/ }),

/***/ "7ggv":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/logical");

/***/ }),

/***/ "8cZr":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("VDXt")


/***/ }),

/***/ "98FW":
/***/ (function(module, exports) {

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

module.exports = _objectWithoutPropertiesLoose;

/***/ }),

/***/ "A6cL":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return normalizeValueWithProperty; });
/* harmony import */ var _modules_unitlessNumbers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("ou6r");
/* harmony import */ var _modules_normalizeColor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cdkR");
var colorProps={backgroundColor:true,borderColor:true,borderTopColor:true,borderRightColor:true,borderBottomColor:true,borderLeftColor:true,color:true,shadowColor:true,textDecorationColor:true,textShadowColor:true};function normalizeValueWithProperty(value,property){var returnValue=value;if((property==null||!_modules_unitlessNumbers__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"][property])&&typeof value==='number'){returnValue=value+"px";}else if(property!=null&&colorProps[property]){returnValue=Object(_modules_normalizeColor__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(value);}return returnValue;}

/***/ }),

/***/ "AXZJ":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
exports.__esModule=true;exports.htmlEscapeJsonString=htmlEscapeJsonString;// This utility is based on https://github.com/zertosh/htmlescape
// License: https://github.com/zertosh/htmlescape/blob/0527ca7156a524d256101bb310a9f970f63078ad/LICENSE
const ESCAPE_LOOKUP={'&':'\\u0026','>':'\\u003e','<':'\\u003c','\u2028':'\\u2028','\u2029':'\\u2029'};const ESCAPE_REGEX=/[&><\u2028\u2029]/g;function htmlEscapeJsonString(str){return str.replace(ESCAPE_REGEX,match=>ESCAPE_LOOKUP[match]);}
//# sourceMappingURL=htmlescape.js.map

/***/ }),

/***/ "AroE":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "C+bE":
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "CPM0":
/***/ (function(module, exports, __webpack_require__) {

var objectWithoutPropertiesLoose = __webpack_require__("98FW");

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

module.exports = _objectWithoutProperties;

/***/ }),

/***/ "CSmu":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return useMergeRefs; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mergeRefs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("VKdU");
function useMergeRefs(){for(var _len=arguments.length,args=new Array(_len),_key=0;_key<_len;_key++){args[_key]=arguments[_key];}return react__WEBPACK_IMPORTED_MODULE_0__["useMemo"](function(){return _mergeRefs__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].apply(void 0,args);},[].concat(args));}

/***/ }),

/***/ "DZbo":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("faye");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_0__);
/* harmony default export */ __webpack_exports__["a"] = (react_dom__WEBPACK_IMPORTED_MODULE_0__["render"]);

/***/ }),

/***/ "DjSU":
/***/ (function(module, exports) {

module.exports = require("normalize-css-color");

/***/ }),

/***/ "DqTX":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
exports.__esModule=true;exports.default=initHeadManager;exports.DOMAttributeNames=void 0;var DOMAttributeNames={acceptCharset:'accept-charset',className:'class',htmlFor:'for',httpEquiv:'http-equiv',noModule:'noModule'};exports.DOMAttributeNames=DOMAttributeNames;function reactElementToDOM(_ref){var type=_ref.type,props=_ref.props;var el=document.createElement(type);for(var p in props){if(!props.hasOwnProperty(p))continue;if(p==='children'||p==='dangerouslySetInnerHTML')continue;if(props[p]===undefined)continue;var attr=DOMAttributeNames[p]||p.toLowerCase();if(type==='script'&&(attr==='async'||attr==='defer'||attr==='noModule')){;el[attr]=!!props[p];}else{el.setAttribute(attr,props[p]);}}var children=props.children,dangerouslySetInnerHTML=props.dangerouslySetInnerHTML;if(dangerouslySetInnerHTML){el.innerHTML=dangerouslySetInnerHTML.__html||'';}else if(children){el.textContent=typeof children==='string'?children:Array.isArray(children)?children.join(''):'';}return el;}function updateElements(type,components){var headEl=document.getElementsByTagName('head')[0];var headCountEl=headEl.querySelector('meta[name=next-head-count]');if(false){}var headCount=Number(headCountEl.content);var oldTags=[];for(var i=0,j=headCountEl.previousElementSibling;i<headCount;i++,j=j.previousElementSibling){if(j.tagName.toLowerCase()===type){oldTags.push(j);}}var newTags=components.map(reactElementToDOM).filter(function(newTag){for(var k=0,len=oldTags.length;k<len;k++){var oldTag=oldTags[k];if(oldTag.isEqualNode(newTag)){oldTags.splice(k,1);return false;}}return true;});oldTags.forEach(function(t){return t.parentNode.removeChild(t);});newTags.forEach(function(t){return headEl.insertBefore(t,headCountEl);});headCountEl.content=(headCount-oldTags.length+newTags.length).toString();}function initHeadManager(){var updatePromise=null;return{mountedInstances:new Set(),updateHead:function updateHead(head){var promise=updatePromise=Promise.resolve().then(function(){if(promise!==updatePromise)return;updatePromise=null;var tags={};head.forEach(function(h){if(h.type==='link'&&h.props['data-optimized-fonts']&&!document.querySelector("style[data-href=\""+h.props['data-href']+"\"]")){h.props.href=h.props['data-href'];h.props['data-href']=undefined;}var components=tags[h.type]||[];components.push(h);tags[h.type]=components;});var titleComponent=tags.title?tags.title[0]:null;var title='';if(titleComponent){var children=titleComponent.props.children;title=typeof children==='string'?children:Array.isArray(children)?children.join(''):'';}if(title!==document.title)document.title=title;['meta','base','link','style','script'].forEach(function(type){updateElements(type,tags[type]||[]);});});}};}

/***/ }),

/***/ "Ecjf":
/***/ (function(module, exports) {

module.exports = require("fbjs/lib/invariant");

/***/ }),

/***/ "GsEv":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = __webpack_require__("luDK");

function emptyFunction() {}
function emptyFunctionWithReset() {}
emptyFunctionWithReset.resetWarningCache = emptyFunction;

module.exports = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  };
  shim.isRequired = shim;
  function getShim() {
    return shim;
  };
  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    elementType: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim,

    checkPropTypes: emptyFunctionWithReset,
    resetWarningCache: emptyFunction
  };

  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),

/***/ "H+61":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _classCallCheck; });
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

/***/ }),

/***/ "HSq1":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ useResponderEvents; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: external "fbjs/lib/ExecutionEnvironment"
var ExecutionEnvironment_ = __webpack_require__("L1DD");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/modules/getBoundingClientRect/index.js
var getBoundingClientRect = __webpack_require__("Q/JM");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/useResponderEvents/ResponderEventTypes.js
var BLUR='blur';var CONTEXT_MENU='contextmenu';var FOCUS_OUT='focusout';var MOUSE_DOWN='mousedown';var MOUSE_MOVE='mousemove';var MOUSE_UP='mouseup';var MOUSE_CANCEL='dragstart';var TOUCH_START='touchstart';var TOUCH_MOVE='touchmove';var TOUCH_END='touchend';var TOUCH_CANCEL='touchcancel';var SCROLL='scroll';var SELECT='select';var SELECTION_CHANGE='selectionchange';function isStartish(eventType){return eventType===TOUCH_START||eventType===MOUSE_DOWN;}function isMoveish(eventType){return eventType===TOUCH_MOVE||eventType===MOUSE_MOVE;}function isEndish(eventType){return eventType===TOUCH_END||eventType===MOUSE_UP||isCancelish(eventType);}function isCancelish(eventType){return eventType===TOUCH_CANCEL||eventType===MOUSE_CANCEL;}function isScroll(eventType){return eventType===SCROLL;}function isSelectionChange(eventType){return eventType===SELECT||eventType===SELECTION_CHANGE;}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/useResponderEvents/ResponderTouchHistoryStore.js
var __DEV__="production"!=='production';var MAX_TOUCH_BANK=20;var touchBank=[];var touchHistory={touchBank:touchBank,numberActiveTouches:0,indexOfSingleActiveTouch:-1,mostRecentTimeStamp:0};function timestampForTouch(touch){return touch.timeStamp||touch.timestamp;}function createTouchRecord(touch){return{touchActive:true,startPageX:touch.pageX,startPageY:touch.pageY,startTimeStamp:timestampForTouch(touch),currentPageX:touch.pageX,currentPageY:touch.pageY,currentTimeStamp:timestampForTouch(touch),previousPageX:touch.pageX,previousPageY:touch.pageY,previousTimeStamp:timestampForTouch(touch)};}function resetTouchRecord(touchRecord,touch){touchRecord.touchActive=true;touchRecord.startPageX=touch.pageX;touchRecord.startPageY=touch.pageY;touchRecord.startTimeStamp=timestampForTouch(touch);touchRecord.currentPageX=touch.pageX;touchRecord.currentPageY=touch.pageY;touchRecord.currentTimeStamp=timestampForTouch(touch);touchRecord.previousPageX=touch.pageX;touchRecord.previousPageY=touch.pageY;touchRecord.previousTimeStamp=timestampForTouch(touch);}function getTouchIdentifier(_ref){var identifier=_ref.identifier;if(identifier==null){console.error('Touch object is missing identifier.');}if(__DEV__){if(identifier>MAX_TOUCH_BANK){console.error('Touch identifier %s is greater than maximum supported %s which causes '+'performance issues backfilling array locations for all of the indices.',identifier,MAX_TOUCH_BANK);}}return identifier;}function recordTouchStart(touch){var identifier=getTouchIdentifier(touch);var touchRecord=touchBank[identifier];if(touchRecord){resetTouchRecord(touchRecord,touch);}else{touchBank[identifier]=createTouchRecord(touch);}touchHistory.mostRecentTimeStamp=timestampForTouch(touch);}function recordTouchMove(touch){var touchRecord=touchBank[getTouchIdentifier(touch)];if(touchRecord){touchRecord.touchActive=true;touchRecord.previousPageX=touchRecord.currentPageX;touchRecord.previousPageY=touchRecord.currentPageY;touchRecord.previousTimeStamp=touchRecord.currentTimeStamp;touchRecord.currentPageX=touch.pageX;touchRecord.currentPageY=touch.pageY;touchRecord.currentTimeStamp=timestampForTouch(touch);touchHistory.mostRecentTimeStamp=timestampForTouch(touch);}else{console.warn('Cannot record touch move without a touch start.\n',"Touch Move: "+printTouch(touch)+"\n","Touch Bank: "+printTouchBank());}}function recordTouchEnd(touch){var touchRecord=touchBank[getTouchIdentifier(touch)];if(touchRecord){touchRecord.touchActive=false;touchRecord.previousPageX=touchRecord.currentPageX;touchRecord.previousPageY=touchRecord.currentPageY;touchRecord.previousTimeStamp=touchRecord.currentTimeStamp;touchRecord.currentPageX=touch.pageX;touchRecord.currentPageY=touch.pageY;touchRecord.currentTimeStamp=timestampForTouch(touch);touchHistory.mostRecentTimeStamp=timestampForTouch(touch);}else{console.warn('Cannot record touch end without a touch start.\n',"Touch End: "+printTouch(touch)+"\n","Touch Bank: "+printTouchBank());}}function printTouch(touch){return JSON.stringify({identifier:touch.identifier,pageX:touch.pageX,pageY:touch.pageY,timestamp:timestampForTouch(touch)});}function printTouchBank(){var printed=JSON.stringify(touchBank.slice(0,MAX_TOUCH_BANK));if(touchBank.length>MAX_TOUCH_BANK){printed+=' (original size: '+touchBank.length+')';}return printed;}var ResponderTouchHistoryStore={recordTouchTrack:function recordTouchTrack(topLevelType,nativeEvent){if(isMoveish(topLevelType)){nativeEvent.changedTouches.forEach(recordTouchMove);}else if(isStartish(topLevelType)){nativeEvent.changedTouches.forEach(recordTouchStart);touchHistory.numberActiveTouches=nativeEvent.touches.length;if(touchHistory.numberActiveTouches===1){touchHistory.indexOfSingleActiveTouch=nativeEvent.touches[0].identifier;}}else if(isEndish(topLevelType)){nativeEvent.changedTouches.forEach(recordTouchEnd);touchHistory.numberActiveTouches=nativeEvent.touches.length;if(touchHistory.numberActiveTouches===1){for(var i=0;i<touchBank.length;i++){var touchTrackToCheck=touchBank[i];if(touchTrackToCheck!=null&&touchTrackToCheck.touchActive){touchHistory.indexOfSingleActiveTouch=i;break;}}if(__DEV__){var activeRecord=touchBank[touchHistory.indexOfSingleActiveTouch];if(!(activeRecord!=null&&activeRecord.touchActive)){console.error('Cannot find single active touch.');}}}}},touchHistory:touchHistory};/* harmony default export */ var useResponderEvents_ResponderTouchHistoryStore = (ResponderTouchHistoryStore);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/useResponderEvents/createResponderEvent.js
var emptyFunction=function emptyFunction(){};var emptyObject={};var emptyArray=[];function normalizeIdentifier(identifier){return identifier>20?identifier%20:identifier;}function createResponderEvent(domEvent){var rect;var propagationWasStopped=false;var changedTouches;var touches;var domEventChangedTouches=domEvent.changedTouches;var domEventType=domEvent.type;var metaKey=domEvent.metaKey===true;var shiftKey=domEvent.shiftKey===true;var force=domEventChangedTouches&&domEventChangedTouches[0].force||0;var identifier=normalizeIdentifier(domEventChangedTouches&&domEventChangedTouches[0].identifier||0);var clientX=domEventChangedTouches&&domEventChangedTouches[0].clientX||domEvent.clientX;var clientY=domEventChangedTouches&&domEventChangedTouches[0].clientY||domEvent.clientY;var pageX=domEventChangedTouches&&domEventChangedTouches[0].pageX||domEvent.pageX;var pageY=domEventChangedTouches&&domEventChangedTouches[0].pageY||domEvent.pageY;var preventDefault=typeof domEvent.preventDefault==='function'?domEvent.preventDefault.bind(domEvent):emptyFunction;var timestamp=domEvent.timeStamp;function normalizeTouches(touches){return Array.prototype.slice.call(touches).map(function(touch){return{force:touch.force,identifier:normalizeIdentifier(touch.identifier),get locationX(){return locationX(touch.clientX);},get locationY(){return locationY(touch.clientY);},pageX:touch.pageX,pageY:touch.pageY,target:touch.target,timestamp:timestamp};});}if(domEventChangedTouches!=null){changedTouches=normalizeTouches(domEventChangedTouches);touches=normalizeTouches(domEvent.touches);}else{var emulatedTouches=[{force:force,identifier:identifier,get locationX(){return locationX(clientX);},get locationY(){return locationY(clientY);},pageX:pageX,pageY:pageY,target:domEvent.target,timestamp:timestamp}];changedTouches=emulatedTouches;touches=domEventType==='mouseup'||domEventType==='dragstart'?emptyArray:emulatedTouches;}var responderEvent={bubbles:true,cancelable:true,currentTarget:null,defaultPrevented:domEvent.defaultPrevented,dispatchConfig:emptyObject,eventPhase:domEvent.eventPhase,isDefaultPrevented:function isDefaultPrevented(){return domEvent.defaultPrevented;},isPropagationStopped:function isPropagationStopped(){return propagationWasStopped;},isTrusted:domEvent.isTrusted,nativeEvent:{altKey:false,ctrlKey:false,metaKey:metaKey,shiftKey:shiftKey,changedTouches:changedTouches,force:force,identifier:identifier,get locationX(){return locationX(clientX);},get locationY(){return locationY(clientY);},pageX:pageX,pageY:pageY,target:domEvent.target,timestamp:timestamp,touches:touches,type:domEventType},persist:emptyFunction,preventDefault:preventDefault,stopPropagation:function stopPropagation(){propagationWasStopped=true;},target:domEvent.target,timeStamp:timestamp,touchHistory:useResponderEvents_ResponderTouchHistoryStore.touchHistory};function locationX(x){rect=rect||Object(getBoundingClientRect["a" /* default */])(responderEvent.currentTarget);if(rect){return x-rect.left;}}function locationY(y){rect=rect||Object(getBoundingClientRect["a" /* default */])(responderEvent.currentTarget);if(rect){return y-rect.top;}}return responderEvent;}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/isSelectionValid/index.js
function isSelectionValid(){var selection=window.getSelection();var string=selection.toString();var anchorNode=selection.anchorNode;var focusNode=selection.focusNode;var isTextNode=anchorNode&&anchorNode.nodeType===window.Node.TEXT_NODE||focusNode&&focusNode.nodeType===window.Node.TEXT_NODE;return string.length>=1&&string!=='\n'&&isTextNode;}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/useResponderEvents/utils.js
var keyName='__reactResponderId';function getEventPath(domEvent){if(domEvent.type==='selectionchange'){var target=window.getSelection().anchorNode;return composedPathFallback(target);}else{var path=domEvent.composedPath!=null?domEvent.composedPath():composedPathFallback(domEvent.target);return path;}}function composedPathFallback(target){var path=[];while(target!=null&&target!==document.body){path.push(target);target=target.parentNode;}return path;}function getResponderId(node){if(node!=null){return node[keyName];}return null;}function setResponderId(node,id){if(node!=null){node[keyName]=id;}}function getResponderPaths(domEvent){var idPath=[];var nodePath=[];var eventPath=getEventPath(domEvent);for(var i=0;i<eventPath.length;i++){var node=eventPath[i];var id=getResponderId(node);if(id!=null){idPath.push(id);nodePath.push(node);}}return{idPath:idPath,nodePath:nodePath};}function getLowestCommonAncestor(pathA,pathB){var pathALength=pathA.length;var pathBLength=pathB.length;if(pathALength===0||pathBLength===0||pathA[pathALength-1]!==pathB[pathBLength-1]){return null;}var itemA=pathA[0];var indexA=0;var itemB=pathB[0];var indexB=0;if(pathALength-pathBLength>0){indexA=pathALength-pathBLength;itemA=pathA[indexA];pathALength=pathBLength;}if(pathBLength-pathALength>0){indexB=pathBLength-pathALength;itemB=pathB[indexB];pathBLength=pathALength;}var depth=pathALength;while(depth--){if(itemA===itemB){return itemA;}itemA=pathA[indexA++];itemB=pathB[indexB++];}return null;}function hasTargetTouches(target,touches){if(!touches||touches.length===0){return false;}for(var i=0;i<touches.length;i++){var node=touches[i].target;if(node!=null){if(target.contains(node)){return true;}}}return false;}function hasValidSelection(domEvent){if(domEvent.type==='selectionchange'){return isSelectionValid();}return domEvent.type==='select';}function isPrimaryPointerDown(domEvent){var altKey=domEvent.altKey,button=domEvent.button,buttons=domEvent.buttons,ctrlKey=domEvent.ctrlKey,type=domEvent.type;var isTouch=type==='touchstart'||type==='touchmove';var isPrimaryMouseDown=type==='mousedown'&&(button===0||buttons===1);var isPrimaryMouseMove=type==='mousemove'&&buttons===1;var noModifiers=altKey===false&&ctrlKey===false;if(isTouch||isPrimaryMouseDown&&noModifiers||isPrimaryMouseMove&&noModifiers){return true;}return false;}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/useResponderEvents/ResponderSystem.js
var ResponderSystem_emptyObject={};var startRegistration=['onStartShouldSetResponderCapture','onStartShouldSetResponder',{bubbles:true}];var moveRegistration=['onMoveShouldSetResponderCapture','onMoveShouldSetResponder',{bubbles:true}];var scrollRegistration=['onScrollShouldSetResponderCapture','onScrollShouldSetResponder',{bubbles:false}];var shouldSetResponderEvents={touchstart:startRegistration,mousedown:startRegistration,touchmove:moveRegistration,mousemove:moveRegistration,scroll:scrollRegistration};var emptyResponder={id:null,idPath:null,node:null};var responderListenersMap=new Map();var isEmulatingMouseEvents=false;var trackedTouchCount=0;var currentResponder={id:null,node:null,idPath:null};function changeCurrentResponder(responder){currentResponder=responder;}function getResponderConfig(id){var config=responderListenersMap.get(id);return config!=null?config:ResponderSystem_emptyObject;}function eventListener(domEvent){var eventType=domEvent.type;var eventTarget=domEvent.target;if(eventType==='touchstart'){isEmulatingMouseEvents=true;}if(eventType==='touchmove'||trackedTouchCount>1){isEmulatingMouseEvents=false;}if(eventType==='mousedown'&&isEmulatingMouseEvents||eventType==='mousemove'&&isEmulatingMouseEvents||eventType==='mousemove'&&trackedTouchCount<1){return;}if(isEmulatingMouseEvents&&eventType==='mouseup'){if(trackedTouchCount===0){isEmulatingMouseEvents=false;}return;}var isStartEvent=isStartish(eventType)&&isPrimaryPointerDown(domEvent);var isMoveEvent=isMoveish(eventType);var isEndEvent=isEndish(eventType);var isScrollEvent=isScroll(eventType);var isSelectionChangeEvent=isSelectionChange(eventType);var responderEvent=createResponderEvent(domEvent);if(isStartEvent||isMoveEvent||isEndEvent){if(domEvent.touches){trackedTouchCount=domEvent.touches.length;}else{if(isStartEvent){trackedTouchCount=1;}else if(isEndEvent){trackedTouchCount=0;}}useResponderEvents_ResponderTouchHistoryStore.recordTouchTrack(eventType,responderEvent.nativeEvent);}var eventPaths=getResponderPaths(domEvent);var wasNegotiated=false;var wantsResponder;if(isStartEvent||isMoveEvent||isScrollEvent&&trackedTouchCount>0){var currentResponderIdPath=currentResponder.idPath;var eventIdPath=eventPaths.idPath;if(currentResponderIdPath!=null&&eventIdPath!=null){var lowestCommonAncestor=getLowestCommonAncestor(currentResponderIdPath,eventIdPath);if(lowestCommonAncestor!=null){var indexOfLowestCommonAncestor=eventIdPath.indexOf(lowestCommonAncestor);var index=indexOfLowestCommonAncestor+(lowestCommonAncestor===currentResponder.id?1:0);eventPaths={idPath:eventIdPath.slice(index),nodePath:eventPaths.nodePath.slice(index)};}else{eventPaths=null;}}if(eventPaths!=null){wantsResponder=findWantsResponder(eventPaths,domEvent,responderEvent);if(wantsResponder!=null){attemptTransfer(responderEvent,wantsResponder);wasNegotiated=true;}}}if(currentResponder.id!=null&&currentResponder.node!=null){var _currentResponder=currentResponder,id=_currentResponder.id,node=_currentResponder.node;var _getResponderConfig=getResponderConfig(id),onResponderStart=_getResponderConfig.onResponderStart,onResponderMove=_getResponderConfig.onResponderMove,onResponderEnd=_getResponderConfig.onResponderEnd,onResponderRelease=_getResponderConfig.onResponderRelease,onResponderTerminate=_getResponderConfig.onResponderTerminate,onResponderTerminationRequest=_getResponderConfig.onResponderTerminationRequest;responderEvent.bubbles=false;responderEvent.cancelable=false;responderEvent.currentTarget=node;if(isStartEvent){if(onResponderStart!=null){responderEvent.dispatchConfig.registrationName='onResponderStart';onResponderStart(responderEvent);}}else if(isMoveEvent){if(onResponderMove!=null){responderEvent.dispatchConfig.registrationName='onResponderMove';onResponderMove(responderEvent);}}else{var isTerminateEvent=isCancelish(eventType)||eventType==='contextmenu'||eventType==='blur'&&eventTarget===window||eventType==='blur'&&eventTarget.contains(node)&&domEvent.relatedTarget!==node||isScrollEvent&&trackedTouchCount===0||isScrollEvent&&eventTarget.contains(node)&&eventTarget!==node||isSelectionChangeEvent&&hasValidSelection(domEvent);var isReleaseEvent=isEndEvent&&!isTerminateEvent&&!hasTargetTouches(node,domEvent.touches);if(isEndEvent){if(onResponderEnd!=null){responderEvent.dispatchConfig.registrationName='onResponderEnd';onResponderEnd(responderEvent);}}if(isReleaseEvent){if(onResponderRelease!=null){responderEvent.dispatchConfig.registrationName='onResponderRelease';onResponderRelease(responderEvent);}changeCurrentResponder(emptyResponder);}if(isTerminateEvent){var shouldTerminate=true;if(eventType==='contextmenu'||eventType==='scroll'||eventType==='selectionchange'){if(wasNegotiated){shouldTerminate=false;}else if(onResponderTerminationRequest!=null){responderEvent.dispatchConfig.registrationName='onResponderTerminationRequest';if(onResponderTerminationRequest(responderEvent)===false){shouldTerminate=false;}}}if(shouldTerminate){if(onResponderTerminate!=null){responderEvent.dispatchConfig.registrationName='onResponderTerminate';onResponderTerminate(responderEvent);}changeCurrentResponder(emptyResponder);isEmulatingMouseEvents=false;trackedTouchCount=0;}}}}}function findWantsResponder(eventPaths,domEvent,responderEvent){var shouldSetCallbacks=shouldSetResponderEvents[domEvent.type];if(shouldSetCallbacks!=null){var idPath=eventPaths.idPath,nodePath=eventPaths.nodePath;var shouldSetCallbackCaptureName=shouldSetCallbacks[0];var shouldSetCallbackBubbleName=shouldSetCallbacks[1];var bubbles=shouldSetCallbacks[2].bubbles;var check=function check(id,node,callbackName){var config=getResponderConfig(id);var shouldSetCallback=config[callbackName];if(shouldSetCallback!=null){responderEvent.currentTarget=node;if(shouldSetCallback(responderEvent)===true){var prunedIdPath=idPath.slice(idPath.indexOf(id));return{id:id,node:node,idPath:prunedIdPath};}}};for(var i=idPath.length-1;i>=0;i--){var id=idPath[i];var node=nodePath[i];var result=check(id,node,shouldSetCallbackCaptureName);if(result!=null){return result;}if(responderEvent.isPropagationStopped()===true){return;}}if(bubbles){for(var _i=0;_i<idPath.length;_i++){var _id=idPath[_i];var _node=nodePath[_i];var _result=check(_id,_node,shouldSetCallbackBubbleName);if(_result!=null){return _result;}if(responderEvent.isPropagationStopped()===true){return;}}}else{var _id2=idPath[0];var _node2=nodePath[0];var target=domEvent.target;if(target===_node2){return check(_id2,_node2,shouldSetCallbackBubbleName);}}}}function attemptTransfer(responderEvent,wantsResponder){var _currentResponder2=currentResponder,currentId=_currentResponder2.id,currentNode=_currentResponder2.node;var id=wantsResponder.id,node=wantsResponder.node;var _getResponderConfig2=getResponderConfig(id),onResponderGrant=_getResponderConfig2.onResponderGrant,onResponderReject=_getResponderConfig2.onResponderReject;responderEvent.bubbles=false;responderEvent.cancelable=false;responderEvent.currentTarget=node;if(currentId==null){if(onResponderGrant!=null){responderEvent.currentTarget=node;responderEvent.dispatchConfig.registrationName='onResponderGrant';onResponderGrant(responderEvent);}changeCurrentResponder(wantsResponder);}else{var _getResponderConfig3=getResponderConfig(currentId),onResponderTerminate=_getResponderConfig3.onResponderTerminate,onResponderTerminationRequest=_getResponderConfig3.onResponderTerminationRequest;var allowTransfer=true;if(onResponderTerminationRequest!=null){responderEvent.currentTarget=currentNode;responderEvent.dispatchConfig.registrationName='onResponderTerminationRequest';if(onResponderTerminationRequest(responderEvent)===false){allowTransfer=false;}}if(allowTransfer){if(onResponderTerminate!=null){responderEvent.currentTarget=currentNode;responderEvent.dispatchConfig.registrationName='onResponderTerminate';onResponderTerminate(responderEvent);}if(onResponderGrant!=null){responderEvent.currentTarget=node;responderEvent.dispatchConfig.registrationName='onResponderGrant';onResponderGrant(responderEvent);}changeCurrentResponder(wantsResponder);}else{if(onResponderReject!=null){responderEvent.currentTarget=node;responderEvent.dispatchConfig.registrationName='onResponderReject';onResponderReject(responderEvent);}}}}var documentEventsCapturePhase=['blur','scroll'];var documentEventsBubblePhase=['mousedown','mousemove','mouseup','dragstart','touchstart','touchmove','touchend','touchcancel','contextmenu','select','selectionchange'];function attachListeners(){if(ExecutionEnvironment_["canUseDOM"]&&window.__reactResponderSystemActive==null){window.addEventListener('blur',eventListener);documentEventsBubblePhase.forEach(function(eventType){document.addEventListener(eventType,eventListener);});documentEventsCapturePhase.forEach(function(eventType){document.addEventListener(eventType,eventListener,true);});window.__reactResponderSystemActive=true;}}function addNode(id,node,config){setResponderId(node,id);responderListenersMap.set(id,config);}function removeNode(id){if(currentResponder.id===id){terminateResponder();}if(responderListenersMap.has(id)){responderListenersMap["delete"](id);}}function terminateResponder(){var _currentResponder3=currentResponder,id=_currentResponder3.id,node=_currentResponder3.node;if(id!=null&&node!=null){var _getResponderConfig4=getResponderConfig(id),onResponderTerminate=_getResponderConfig4.onResponderTerminate;if(onResponderTerminate!=null){var event=createResponderEvent({});event.currentTarget=node;onResponderTerminate(event);}changeCurrentResponder(emptyResponder);}isEmulatingMouseEvents=false;trackedTouchCount=0;}function getResponderNode(){return currentResponder.node;}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/useResponderEvents/index.js
var useResponderEvents_emptyObject={};var idCounter=0;function useStable(getInitialValue){var ref=external_react_["useRef"](null);if(ref.current==null){ref.current=getInitialValue();}return ref.current;}function useResponderEvents(hostRef,config){if(config===void 0){config=useResponderEvents_emptyObject;}var id=useStable(function(){return idCounter++;});var isAttachedRef=external_react_["useRef"](false);external_react_["useEffect"](function(){attachListeners();return function(){removeNode(id);};},[id]);external_react_["useEffect"](function(){var _config=config,onMoveShouldSetResponder=_config.onMoveShouldSetResponder,onMoveShouldSetResponderCapture=_config.onMoveShouldSetResponderCapture,onScrollShouldSetResponder=_config.onScrollShouldSetResponder,onScrollShouldSetResponderCapture=_config.onScrollShouldSetResponderCapture,onSelectionChangeShouldSetResponder=_config.onSelectionChangeShouldSetResponder,onSelectionChangeShouldSetResponderCapture=_config.onSelectionChangeShouldSetResponderCapture,onStartShouldSetResponder=_config.onStartShouldSetResponder,onStartShouldSetResponderCapture=_config.onStartShouldSetResponderCapture;var requiresResponderSystem=onMoveShouldSetResponder!=null||onMoveShouldSetResponderCapture!=null||onScrollShouldSetResponder!=null||onScrollShouldSetResponderCapture!=null||onSelectionChangeShouldSetResponder!=null||onSelectionChangeShouldSetResponderCapture!=null||onStartShouldSetResponder!=null||onStartShouldSetResponderCapture!=null;var node=hostRef.current;if(requiresResponderSystem){addNode(id,node,config);isAttachedRef.current=true;}else if(isAttachedRef.current){removeNode(id);isAttachedRef.current=false;}},[config,hostRef,id]);external_react_["useDebugValue"]({isResponder:hostRef.current===getResponderNode()});external_react_["useDebugValue"](config);}

/***/ }),

/***/ "Iu5r":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _ReactNativePropRegistry__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("XFth");
/* harmony import */ var fbjs_lib_invariant__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("Ecjf");
/* harmony import */ var fbjs_lib_invariant__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fbjs_lib_invariant__WEBPACK_IMPORTED_MODULE_1__);
function getStyle(style){if(typeof style==='number'){return _ReactNativePropRegistry__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].getByID(style);}return style;}function flattenStyle(style){if(!style){return undefined;}if(false){}if(!Array.isArray(style)){return getStyle(style);}var result={};for(var i=0,styleLength=style.length;i<styleLength;++i){var computedStyle=flattenStyle(style[i]);if(computedStyle){for(var key in computedStyle){var value=computedStyle[key];result[key]=value;}}}return result;}/* harmony default export */ __webpack_exports__["a"] = (flattenStyle);

/***/ }),

/***/ "IybW":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/AccessibilityUtil/isDisabled.js
var isDisabled=function isDisabled(props){return props.disabled||Array.isArray(props.accessibilityStates)&&props.accessibilityStates.indexOf('disabled')>-1;};/* harmony default export */ var AccessibilityUtil_isDisabled = (isDisabled);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/AccessibilityUtil/propsToAriaRole.js
var accessibilityRoleToWebRole={adjustable:'slider',button:'button',header:'heading',image:'img',imagebutton:null,keyboardkey:null,label:null,link:'link',none:'presentation',search:'search',summary:'region',text:null};var propsToAriaRole=function propsToAriaRole(_ref){var accessibilityRole=_ref.accessibilityRole;if(accessibilityRole){var inferredRole=accessibilityRoleToWebRole[accessibilityRole];if(inferredRole!==null){return inferredRole||accessibilityRole;}}};/* harmony default export */ var AccessibilityUtil_propsToAriaRole = (propsToAriaRole);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/AccessibilityUtil/propsToAccessibilityComponent.js
var roleComponents={article:'article',banner:'header',blockquote:'blockquote',code:'code',complementary:'aside',contentinfo:'footer',deletion:'del',emphasis:'em',figure:'figure',insertion:'ins',form:'form',list:'ul',listitem:'li',main:'main',navigation:'nav',region:'section',strong:'strong'};var emptyObject={};var propsToAccessibilityComponent_propsToAccessibilityComponent=function propsToAccessibilityComponent(props){if(props===void 0){props=emptyObject;}if(props.accessibilityRole==='label'){return'label';}var role=AccessibilityUtil_propsToAriaRole(props);if(role){if(role==='heading'){var level=props.accessibilityLevel||props['aria-level'];if(level!=null){return"h"+level;}return'h1';}return roleComponents[role];}};/* harmony default export */ var AccessibilityUtil_propsToAccessibilityComponent = (propsToAccessibilityComponent_propsToAccessibilityComponent);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/AccessibilityUtil/index.js
var AccessibilityUtil={isDisabled:AccessibilityUtil_isDisabled,propsToAccessibilityComponent:AccessibilityUtil_propsToAccessibilityComponent,propsToAriaRole:AccessibilityUtil_propsToAriaRole};/* harmony default export */ var modules_AccessibilityUtil = __webpack_exports__["a"] = (AccessibilityUtil);

/***/ }),

/***/ "J3ww":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/crossFade");

/***/ }),

/***/ "KCPX":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return defaultProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return accessibilityProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return clickProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return focusProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return keyboardProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return mouseProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return touchProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return styleProps; });
var defaultProps={children:true,dataSet:true,nativeID:true,ref:true,suppressHydrationWarning:true,testID:true};var accessibilityProps={accessibilityActiveDescendant:true,accessibilityAtomic:true,accessibilityAutoComplete:true,accessibilityBusy:true,accessibilityChecked:true,accessibilityColumnCount:true,accessibilityColumnIndex:true,accessibilityColumnSpan:true,accessibilityControls:true,accessibilityCurrent:true,accessibilityDescribedBy:true,accessibilityDetails:true,accessibilityDisabled:true,accessibilityErrorMessage:true,accessibilityExpanded:true,accessibilityFlowTo:true,accessibilityHasPopup:true,accessibilityHidden:true,accessibilityInvalid:true,accessibilityKeyShortcuts:true,accessibilityLabel:true,accessibilityLabelledBy:true,accessibilityLevel:true,accessibilityLiveRegion:true,accessibilityModal:true,accessibilityMultiline:true,accessibilityMultiSelectable:true,accessibilityOrientation:true,accessibilityOwns:true,accessibilityPlaceholder:true,accessibilityPosInSet:true,accessibilityPressed:true,accessibilityReadOnly:true,accessibilityRequired:true,accessibilityRole:true,accessibilityRoleDescription:true,accessibilityRowCount:true,accessibilityRowIndex:true,accessibilityRowSpan:true,accessibilitySelected:true,accessibilitySetSize:true,accessibilitySort:true,accessibilityValueMax:true,accessibilityValueMin:true,accessibilityValueNow:true,accessibilityValueText:true,dir:true,focusable:true,accessible:true,accessibilityState:true,accessibilityValue:true};var clickProps={onClick:true,onClickCapture:true,onContextMenu:true};var focusProps={onBlur:true,onFocus:true};var keyboardProps={onKeyDown:true,onKeyDownCapture:true,onKeyUp:true,onKeyUpCapture:true};var mouseProps={onMouseDown:true,onMouseEnter:true,onMouseLeave:true,onMouseMove:true,onMouseOver:true,onMouseOut:true,onMouseUp:true};var touchProps={onTouchCancel:true,onTouchCancelCapture:true,onTouchEnd:true,onTouchEndCapture:true,onTouchMove:true,onTouchMoveCapture:true,onTouchStart:true,onTouchStartCapture:true};var styleProps={classList:true,style:true};

/***/ }),

/***/ "KckH":
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__("7eYB");

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

module.exports = _unsupportedIterableToArray;

/***/ }),

/***/ "L1DD":
/***/ (function(module, exports) {

module.exports = require("fbjs/lib/ExecutionEnvironment");

/***/ }),

/***/ "LnWa":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/cursor");

/***/ }),

/***/ "NRdY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("faye");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_0__);
/* harmony default export */ __webpack_exports__["a"] = (react_dom__WEBPACK_IMPORTED_MODULE_0__["unmountComponentAtNode"]);

/***/ }),

/***/ "PqPU":
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

module.exports = _arrayWithHoles;

/***/ }),

/***/ "Q/JM":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var getBoundingClientRect=function getBoundingClientRect(node){if(node!=null){var isElement=node.nodeType===1;if(isElement&&typeof node.getBoundingClientRect==='function'){return node.getBoundingClientRect();}}};/* harmony default export */ __webpack_exports__["a"] = (getBoundingClientRect);

/***/ }),

/***/ "Q3tu":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var TextAncestorContext=Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(false);/* harmony default export */ __webpack_exports__["a"] = (TextAncestorContext);

/***/ }),

/***/ "S+yN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("L1DD");
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__);
var doLeftAndRightSwapInRTL=true;var isPreferredLanguageRTL=false;var isRTLAllowed=true;var isRTLForced=false;var isRTL=function isRTL(){if(isRTLForced){return true;}return isRTLAllowed&&isPreferredLanguageRTL;};var onDirectionChange=function onDirectionChange(){if(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0___default.a.canUseDOM){if(document.documentElement&&document.documentElement.setAttribute){document.documentElement.setAttribute('dir',isRTL()?'rtl':'ltr');}}};var I18nManager={allowRTL:function allowRTL(bool){isRTLAllowed=bool;onDirectionChange();},forceRTL:function forceRTL(bool){isRTLForced=bool;onDirectionChange();},getConstants:function getConstants(){return{doLeftAndRightSwapInRTL:doLeftAndRightSwapInRTL,isRTL:isRTL()};},setPreferredLanguageRTL:function setPreferredLanguageRTL(bool){isPreferredLanguageRTL=bool;onDirectionChange();},swapLeftAndRightInRTL:function swapLeftAndRightInRTL(bool){doLeftAndRightSwapInRTL=bool;}};/* harmony default export */ __webpack_exports__["a"] = (I18nManager);

/***/ }),

/***/ "T0f4":
/***/ (function(module, exports) {

function _getPrototypeOf(o) {
  module.exports = _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

module.exports = _getPrototypeOf;

/***/ }),

/***/ "TgPG":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "fbjs/lib/ExecutionEnvironment"
var ExecutionEnvironment_ = __webpack_require__("L1DD");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/createCSSStyleSheet.js
function createCSSStyleSheet(id){if(ExecutionEnvironment_["canUseDOM"]){var element=document.getElementById(id);if(element!=null){return element.sheet;}else{var _element=document.createElement('style');_element.setAttribute('id',id);var head=document.head;if(head){head.insertBefore(_element,head.firstChild);}return _element.sheet;}}else{return null;}}
// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/normalizeValueWithProperty.js
var normalizeValueWithProperty = __webpack_require__("A6cL");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/resolveShadowValue.js
var resolveShadowValue = __webpack_require__("cqcS");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/createCompileableStyle.js
function _objectWithoutPropertiesLoose(source,excluded){if(source==null)return{};var target={};var sourceKeys=Object.keys(source);var key,i;for(i=0;i<sourceKeys.length;i++){key=sourceKeys[i];if(excluded.indexOf(key)>=0)continue;target[key]=source[key];}return target;}var defaultOffset={height:0,width:0};function boxShadowReducer(resolvedStyle,style){var boxShadow=style.boxShadow;var shadow=Object(resolveShadowValue["a" /* default */])(style);if(shadow!=null){resolvedStyle.boxShadow=boxShadow?boxShadow+", "+shadow:shadow;}}function textShadowReducer(resolvedStyle,style){var textShadowColor=style.textShadowColor,textShadowOffset=style.textShadowOffset,textShadowRadius=style.textShadowRadius;var _ref=textShadowOffset||defaultOffset,height=_ref.height,width=_ref.width;var radius=textShadowRadius||0;var offsetX=Object(normalizeValueWithProperty["a" /* default */])(width);var offsetY=Object(normalizeValueWithProperty["a" /* default */])(height);var blurRadius=Object(normalizeValueWithProperty["a" /* default */])(radius);var color=Object(normalizeValueWithProperty["a" /* default */])(textShadowColor,'textShadowColor');if(color&&(height!==0||width!==0||radius!==0)&&offsetX!=null&&offsetY!=null&&blurRadius!=null){resolvedStyle.textShadow=offsetX+" "+offsetY+" "+blurRadius+" "+color;}}var createCompileableStyle=function createCompileableStyle(styles){var shadowColor=styles.shadowColor,shadowOffset=styles.shadowOffset,shadowOpacity=styles.shadowOpacity,shadowRadius=styles.shadowRadius,textShadowColor=styles.textShadowColor,textShadowOffset=styles.textShadowOffset,textShadowRadius=styles.textShadowRadius,nextStyles=_objectWithoutPropertiesLoose(styles,["shadowColor","shadowOffset","shadowOpacity","shadowRadius","textShadowColor","textShadowOffset","textShadowRadius"]);if(shadowColor!=null||shadowOffset!=null||shadowOpacity!=null||shadowRadius!=null){boxShadowReducer(nextStyles,styles);}if(textShadowColor!=null||textShadowOffset!=null||textShadowRadius!=null){textShadowReducer(nextStyles,styles);}return nextStyles;};/* harmony default export */ var StyleSheet_createCompileableStyle = (createCompileableStyle);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/createOrderedCSSStyleSheet.js
var slice=Array.prototype.slice;function createOrderedCSSStyleSheet(sheet){var groups={};var selectors={};if(sheet!=null){var group;slice.call(sheet.cssRules).forEach(function(cssRule,i){var cssText=cssRule.cssText;if(cssText.indexOf('stylesheet-group')>-1){group=decodeGroupRule(cssRule);groups[group]={start:i,rules:[cssText]};}else{var selectorText=getSelectorText(cssText);if(selectorText!=null){selectors[selectorText]=true;groups[group].rules.push(cssText);}}});}function sheetInsert(sheet,group,text){var orderedGroups=getOrderedGroups(groups);var groupIndex=orderedGroups.indexOf(group);var nextGroupIndex=groupIndex+1;var nextGroup=orderedGroups[nextGroupIndex];var position=nextGroup!=null&&groups[nextGroup].start!=null?groups[nextGroup].start:sheet.cssRules.length;var isInserted=insertRuleAt(sheet,text,position);if(isInserted){if(groups[group].start==null){groups[group].start=position;}for(var i=nextGroupIndex;i<orderedGroups.length;i+=1){var groupNumber=orderedGroups[i];var previousStart=groups[groupNumber].start||0;groups[groupNumber].start=previousStart+1;}}return isInserted;}var OrderedCSSStyleSheet={getTextContent:function getTextContent(){return getOrderedGroups(groups).map(function(group){var rules=groups[group].rules;return rules.join('\n');}).join('\n');},insert:function insert(cssText,groupValue){var group=Number(groupValue);if(groups[group]==null){var markerRule=encodeGroupRule(group);groups[group]={start:null,rules:[markerRule]};if(sheet!=null){sheetInsert(sheet,group,markerRule);}}var selectorText=getSelectorText(cssText);if(selectorText!=null&&selectors[selectorText]==null){selectors[selectorText]=true;groups[group].rules.push(cssText);if(sheet!=null){var isInserted=sheetInsert(sheet,group,cssText);if(!isInserted){groups[group].rules.pop();}}}}};return OrderedCSSStyleSheet;}function encodeGroupRule(group){return"[stylesheet-group=\""+group+"\"]{}";}function decodeGroupRule(cssRule){return Number(cssRule.selectorText.split(/["']/)[1]);}function getOrderedGroups(obj){return Object.keys(obj).map(Number).sort(function(a,b){return a>b?1:-1;});}var pattern=/\s*([,])\s*/g;function getSelectorText(cssText){var selector=cssText.split('{')[0].trim();return selector!==''?selector.replace(pattern,'$1'):null;}function insertRuleAt(root,cssText,position){try{root.insertRule(cssText,position);return true;}catch(e){return false;}}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/flattenArray/index.js
function flattenArray(array){function flattenDown(array,result){for(var i=0;i<array.length;i++){var value=array[i];if(Array.isArray(value)){flattenDown(value,result);}else if(value!=null&&value!==false){result.push(value);}}return result;}return flattenDown(array,[]);}/* harmony default export */ var modules_flattenArray = (flattenArray);
// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/flattenStyle.js
var flattenStyle = __webpack_require__("Iu5r");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/I18nManager/index.js
var I18nManager = __webpack_require__("S+yN");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/modules/multiplyStyleLengthValue/index.js
var multiplyStyleLengthValue = __webpack_require__("7H9r");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/i18nStyle.js
var emptyObject={};var borderTopLeftRadius='borderTopLeftRadius';var borderTopRightRadius='borderTopRightRadius';var borderBottomLeftRadius='borderBottomLeftRadius';var borderBottomRightRadius='borderBottomRightRadius';var borderLeftColor='borderLeftColor';var borderLeftStyle='borderLeftStyle';var borderLeftWidth='borderLeftWidth';var borderRightColor='borderRightColor';var borderRightStyle='borderRightStyle';var borderRightWidth='borderRightWidth';var right='right';var marginLeft='marginLeft';var marginRight='marginRight';var paddingLeft='paddingLeft';var paddingRight='paddingRight';var left='left';var PROPERTIES_FLIP={borderTopLeftRadius:borderTopRightRadius,borderTopRightRadius:borderTopLeftRadius,borderBottomLeftRadius:borderBottomRightRadius,borderBottomRightRadius:borderBottomLeftRadius,borderLeftColor:borderRightColor,borderLeftStyle:borderRightStyle,borderLeftWidth:borderRightWidth,borderRightColor:borderLeftColor,borderRightStyle:borderLeftStyle,borderRightWidth:borderLeftWidth,left:right,marginLeft:marginRight,marginRight:marginLeft,paddingLeft:paddingRight,paddingRight:paddingLeft,right:left};var PROPERTIES_I18N={borderTopStartRadius:borderTopLeftRadius,borderTopEndRadius:borderTopRightRadius,borderBottomStartRadius:borderBottomLeftRadius,borderBottomEndRadius:borderBottomRightRadius,borderStartColor:borderLeftColor,borderStartStyle:borderLeftStyle,borderStartWidth:borderLeftWidth,borderEndColor:borderRightColor,borderEndStyle:borderRightStyle,borderEndWidth:borderRightWidth,end:right,marginStart:marginLeft,marginEnd:marginRight,paddingStart:paddingLeft,paddingEnd:paddingRight,start:left};var PROPERTIES_VALUE={clear:true,"float":true,textAlign:true};var i18nStyle_additiveInverse=function additiveInverse(value){return Object(multiplyStyleLengthValue["a" /* default */])(value,-1);};var i18nStyle_i18nStyle=function i18nStyle(originalStyle){var _I18nManager$getConst=I18nManager["a" /* default */].getConstants(),doLeftAndRightSwapInRTL=_I18nManager$getConst.doLeftAndRightSwapInRTL,isRTL=_I18nManager$getConst.isRTL;var style=originalStyle||emptyObject;var frozenProps={};var nextStyle={};for(var originalProp in style){if(!Object.prototype.hasOwnProperty.call(style,originalProp)){continue;}var originalValue=style[originalProp];var prop=originalProp;var value=originalValue;if(PROPERTIES_I18N.hasOwnProperty(originalProp)){var convertedProp=PROPERTIES_I18N[originalProp];prop=isRTL?PROPERTIES_FLIP[convertedProp]:convertedProp;}else if(isRTL&&doLeftAndRightSwapInRTL&&PROPERTIES_FLIP[originalProp]){prop=PROPERTIES_FLIP[originalProp];}if(PROPERTIES_VALUE.hasOwnProperty(originalProp)){if(originalValue==='start'){value=isRTL?'right':'left';}else if(originalValue==='end'){value=isRTL?'left':'right';}else if(isRTL&&doLeftAndRightSwapInRTL){if(originalValue==='left'){value='right';}else if(originalValue==='right'){value='left';}}}if(prop==='transitionProperty'){if(PROPERTIES_I18N.hasOwnProperty(value)){var convertedValue=PROPERTIES_I18N[originalValue];value=isRTL?PROPERTIES_FLIP[convertedValue]:convertedValue;}else if(isRTL&&doLeftAndRightSwapInRTL){var flippedValue=PROPERTIES_FLIP[originalValue];if(flippedValue!=null){value=flippedValue;}}}if(isRTL&&prop==='textShadowOffset'){var invertedValue=i18nStyle_additiveInverse(value.width);value.width=invertedValue;nextStyle[prop]=value;}else if(!frozenProps[prop]){nextStyle[prop]=value;}if(PROPERTIES_I18N[originalProp]){frozenProps[prop]=true;}}return nextStyle;};/* harmony default export */ var StyleSheet_i18nStyle = (i18nStyle_i18nStyle);
// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/constants.js
var constants = __webpack_require__("yBWl");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/createReactDOMStyle.js
var createReactDOMStyle_emptyObject={};var supportsCSS3TextDecoration=!ExecutionEnvironment_["canUseDOM"]||window.CSS!=null&&window.CSS.supports!=null&&(window.CSS.supports('text-decoration-line','none')||window.CSS.supports('-webkit-text-decoration-line','none'));var createReactDOMStyle_mapTransform=function mapTransform(transform){var type=Object.keys(transform)[0];var value=transform[type];if(type==='matrix'||type==='matrix3d'){return type+"("+value.join(',')+")";}else{var normalizedValue=Object(normalizeValueWithProperty["a" /* default */])(value,type);return type+"("+normalizedValue+")";}};var resolveTransform=function resolveTransform(resolvedStyle,style){var transform=style.transform;if(Array.isArray(style.transform)){transform=style.transform.map(createReactDOMStyle_mapTransform).join(' ');}resolvedStyle.transform=transform;};var createReactDOMStyle_createReactDOMStyle=function createReactDOMStyle(style){if(!style){return createReactDOMStyle_emptyObject;}var resolvedStyle={};Object.keys(style).sort().forEach(function(prop){var value=Object(normalizeValueWithProperty["a" /* default */])(style[prop],prop);if(value==null){return;}switch(prop){case'elevation':case'overlayColor':case'resizeMode':case'tintColor':{break;}case'aspectRatio':{resolvedStyle[prop]=value.toString();break;}case'backgroundClip':{if(value==='text'){resolvedStyle.backgroundClip=value;resolvedStyle.WebkitBackgroundClip=value;}break;}case'flex':{if(value===-1){resolvedStyle.flexGrow=0;resolvedStyle.flexShrink=1;resolvedStyle.flexBasis='auto';}else{resolvedStyle.flex=value;}break;}case'font':{resolvedStyle[prop]=value.replace('System',constants["e" /* SYSTEM_FONT_STACK */]);break;}case'fontFamily':{if(value.indexOf('System')>-1){var stack=value.split(/,\s*/);stack[stack.indexOf('System')]=constants["e" /* SYSTEM_FONT_STACK */];resolvedStyle[prop]=stack.join(',');}else if(value==='monospace'){resolvedStyle[prop]=constants["a" /* MONOSPACE_FONT_STACK */];}else{resolvedStyle[prop]=value;}break;}case'fontVariant':{if(Array.isArray(value)&&value.length>0){resolvedStyle.fontVariant=value.join(' ');}break;}case'textAlignVertical':{resolvedStyle.verticalAlign=value==='center'?'middle':value;break;}case'textDecorationLine':{if(!supportsCSS3TextDecoration){resolvedStyle.textDecoration=value;}else{resolvedStyle.textDecorationLine=value;}break;}case'transform':case'transformMatrix':{resolveTransform(resolvedStyle,style);break;}case'writingDirection':{resolvedStyle.direction=value;break;}default:{var longFormProperties=constants["d" /* STYLE_SHORT_FORM_EXPANSIONS */][prop];if(longFormProperties){longFormProperties.forEach(function(longForm,i){if(typeof style[longForm]==='undefined'){resolvedStyle[longForm]=value;}});}else{resolvedStyle[prop]=Array.isArray(value)?value.join(','):value;}}}});return resolvedStyle;};/* harmony default export */ var StyleSheet_createReactDOMStyle = (createReactDOMStyle_createReactDOMStyle);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/vendor/hash/index.js
function murmurhash2_32_gc(str,seed){var l=str.length,h=seed^l,i=0,k;while(l>=4){k=str.charCodeAt(i)&0xff|(str.charCodeAt(++i)&0xff)<<8|(str.charCodeAt(++i)&0xff)<<16|(str.charCodeAt(++i)&0xff)<<24;k=(k&0xffff)*0x5bd1e995+(((k>>>16)*0x5bd1e995&0xffff)<<16);k^=k>>>24;k=(k&0xffff)*0x5bd1e995+(((k>>>16)*0x5bd1e995&0xffff)<<16);h=(h&0xffff)*0x5bd1e995+(((h>>>16)*0x5bd1e995&0xffff)<<16)^k;l-=4;++i;}switch(l){case 3:h^=(str.charCodeAt(i+2)&0xff)<<16;case 2:h^=(str.charCodeAt(i+1)&0xff)<<8;case 1:h^=str.charCodeAt(i)&0xff;h=(h&0xffff)*0x5bd1e995+(((h>>>16)*0x5bd1e995&0xffff)<<16);}h^=h>>>13;h=(h&0xffff)*0x5bd1e995+(((h>>>16)*0x5bd1e995&0xffff)<<16);h^=h>>>15;return h>>>0;}var hash=function hash(str){return murmurhash2_32_gc(str,1).toString(36);};/* harmony default export */ var vendor_hash = (hash);
// EXTERNAL MODULE: external "hyphenate-style-name"
var external_hyphenate_style_name_ = __webpack_require__("gwwY");
var external_hyphenate_style_name_default = /*#__PURE__*/__webpack_require__.n(external_hyphenate_style_name_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/createPrefixer"
var createPrefixer_ = __webpack_require__("eNWT");
var createPrefixer_default = /*#__PURE__*/__webpack_require__.n(createPrefixer_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/backgroundClip"
var backgroundClip_ = __webpack_require__("eZI+");
var backgroundClip_default = /*#__PURE__*/__webpack_require__.n(backgroundClip_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/crossFade"
var crossFade_ = __webpack_require__("J3ww");
var crossFade_default = /*#__PURE__*/__webpack_require__.n(crossFade_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/cursor"
var cursor_ = __webpack_require__("LnWa");
var cursor_default = /*#__PURE__*/__webpack_require__.n(cursor_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/filter"
var filter_ = __webpack_require__("kvbm");
var filter_default = /*#__PURE__*/__webpack_require__.n(filter_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/flex"
var flex_ = __webpack_require__("eRNP");
var flex_default = /*#__PURE__*/__webpack_require__.n(flex_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/flexboxIE"
var flexboxIE_ = __webpack_require__("eJic");
var flexboxIE_default = /*#__PURE__*/__webpack_require__.n(flexboxIE_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/flexboxOld"
var flexboxOld_ = __webpack_require__("vTM5");
var flexboxOld_default = /*#__PURE__*/__webpack_require__.n(flexboxOld_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/gradient"
var gradient_ = __webpack_require__("iJv9");
var gradient_default = /*#__PURE__*/__webpack_require__.n(gradient_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/grid"
var grid_ = __webpack_require__("5pvt");
var grid_default = /*#__PURE__*/__webpack_require__.n(grid_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/imageSet"
var imageSet_ = __webpack_require__("bcFB");
var imageSet_default = /*#__PURE__*/__webpack_require__.n(imageSet_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/logical"
var logical_ = __webpack_require__("7ggv");
var logical_default = /*#__PURE__*/__webpack_require__.n(logical_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/position"
var position_ = __webpack_require__("aD8d");
var position_default = /*#__PURE__*/__webpack_require__.n(position_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/sizing"
var sizing_ = __webpack_require__("krLv");
var sizing_default = /*#__PURE__*/__webpack_require__.n(sizing_);

// EXTERNAL MODULE: external "inline-style-prefixer/lib/plugins/transition"
var transition_ = __webpack_require__("+es4");
var transition_default = /*#__PURE__*/__webpack_require__.n(transition_);

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/prefixStyles/static.js
var w=['Webkit'];var m=['Moz'];var ms=['ms'];var wm=['Webkit','Moz'];var wms=['Webkit','ms'];var wmms=['Webkit','Moz','ms'];/* harmony default export */ var prefixStyles_static = ({plugins:[backgroundClip_default.a,crossFade_default.a,cursor_default.a,filter_default.a,flex_default.a,flexboxIE_default.a,flexboxOld_default.a,gradient_default.a,grid_default.a,imageSet_default.a,logical_default.a,position_default.a,sizing_default.a,transition_default.a],prefixMap:{animation:w,animationDelay:w,animationDirection:w,animationFillMode:w,animationDuration:w,animationIterationCount:w,animationName:w,animationPlayState:w,animationTimingFunction:w,appearance:wm,userSelect:wmms,textEmphasisPosition:w,textEmphasis:w,textEmphasisStyle:w,textEmphasisColor:w,boxDecorationBreak:w,clipPath:w,maskImage:w,maskMode:w,maskRepeat:w,maskPosition:w,maskClip:w,maskOrigin:w,maskSize:w,maskComposite:w,mask:w,maskBorderSource:w,maskBorderMode:w,maskBorderSlice:w,maskBorderWidth:w,maskBorderOutset:w,maskBorderRepeat:w,maskBorder:w,maskType:w,textDecorationStyle:w,textDecorationSkip:w,textDecorationLine:w,textDecorationColor:w,filter:w,fontFeatureSettings:w,breakAfter:wmms,breakBefore:wmms,breakInside:wmms,columnCount:wm,columnFill:wm,columnGap:wm,columnRule:wm,columnRuleColor:wm,columnRuleStyle:wm,columnRuleWidth:wm,columns:wm,columnSpan:wm,columnWidth:wm,writingMode:wms,flex:wms,flexBasis:w,flexDirection:wms,flexGrow:w,flexFlow:wms,flexShrink:w,flexWrap:wms,alignContent:w,alignItems:w,alignSelf:w,justifyContent:w,order:w,transform:w,transformOrigin:w,transformOriginX:w,transformOriginY:w,backfaceVisibility:w,perspective:w,perspectiveOrigin:w,transformStyle:w,transformOriginZ:w,backdropFilter:w,fontKerning:w,scrollSnapType:wms,scrollSnapPointsX:wms,scrollSnapPointsY:wms,scrollSnapDestination:wms,scrollSnapCoordinate:wms,shapeImageThreshold:w,shapeImageMargin:w,shapeImageOutside:w,hyphens:wmms,flowInto:wms,flowFrom:wms,regionFragment:wms,textOrientation:w,textAlignLast:m,tabSize:m,wrapFlow:ms,wrapThrough:ms,wrapMargin:ms,touchAction:ms,textSizeAdjust:['ms','Webkit'],borderImage:w,borderImageOutset:w,borderImageRepeat:w,borderImageSlice:w,borderImageSource:w,borderImageWidth:w,transitionDelay:w,transitionDuration:w,transitionProperty:w,transitionTimingFunction:w}});
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/prefixStyles/index.js
var prefixAll=createPrefixer_default()(prefixStyles_static);var prefixInlineStyles=function prefixInlineStyles(style){var prefixedStyles=prefixAll(style);Object.keys(prefixedStyles).forEach(function(prop){var value=prefixedStyles[prop];if(Array.isArray(value)){prefixedStyles[prop]=value[value.length-1];}});return prefixedStyles;};/* harmony default export */ var prefixStyles = (prefixAll);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/compile.js
function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);if(enumerableOnly)symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;});keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=arguments[i]!=null?arguments[i]:{};if(i%2){ownKeys(Object(source),true).forEach(function(key){_defineProperty(target,key,source[key]);});}else if(Object.getOwnPropertyDescriptors){Object.defineProperties(target,Object.getOwnPropertyDescriptors(source));}else{ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}}return target;}function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else{obj[key]=value;}return obj;}function compile_objectWithoutPropertiesLoose(source,excluded){if(source==null)return{};var target={};var sourceKeys=Object.keys(source);var key,i;for(i=0;i<sourceKeys.length;i++){key=sourceKeys[i];if(excluded.indexOf(key)>=0)continue;target[key]=source[key];}return target;}var compile_cache={get:function get(property,value){if(compile_cache[property]!=null&&compile_cache[property].hasOwnProperty(value)&&compile_cache[property][value]!=null){return compile_cache[property][value];}},set:function set(property,value,object){if(compile_cache[property]==null){compile_cache[property]={};}return compile_cache[property][value]=object;}};function atomic(style){return Object.keys(style).sort().reduce(function(acc,property){var value=style[property];if(value!=null){var valueString=stringifyValueWithProperty(value,property);var cachedResult=compile_cache.get(property,valueString);if(cachedResult!=null){var identifier=cachedResult.identifier;acc[identifier]=cachedResult;}else{var _identifier=createIdentifier('r',property,value);var rules=createAtomicRules(_identifier,property,value);var _cachedResult=compile_cache.set(property,valueString,{property:property,value:stringifyValueWithProperty(value,property),identifier:_identifier,rules:rules});acc[_identifier]=_cachedResult;}}return acc;},{});}function classic(style,name){var _ref;var identifier=createIdentifier('css',name,style);var animationKeyframes=style.animationKeyframes,rest=compile_objectWithoutPropertiesLoose(style,["animationKeyframes"]);var rules=[];var selector="."+identifier;var animationName;if(animationKeyframes!=null){var _processKeyframesValu=processKeyframesValue(animationKeyframes),animationNames=_processKeyframesValu.animationNames,keyframesRules=_processKeyframesValu.rules;animationName=animationNames.join(',');rules.push.apply(rules,keyframesRules);}var block=createDeclarationBlock(_objectSpread(_objectSpread({},rest),{},{animationName:animationName}));rules.push(""+selector+block);return _ref={},_ref[identifier]={identifier:identifier,rules:rules},_ref;}function inline(style){return prefixInlineStyles(StyleSheet_createReactDOMStyle(style));}function stringifyValueWithProperty(value,property){var normalizedValue=Object(normalizeValueWithProperty["a" /* default */])(value,property);return typeof normalizedValue!=='string'?JSON.stringify(normalizedValue||''):normalizedValue;}function createAtomicRules(identifier,property,value){var rules=[];var selector="."+identifier;switch(property){case'animationKeyframes':{var _processKeyframesValu2=processKeyframesValue(value),animationNames=_processKeyframesValu2.animationNames,keyframesRules=_processKeyframesValu2.rules;var block=createDeclarationBlock({animationName:animationNames.join(',')});rules.push.apply(rules,[""+selector+block].concat(keyframesRules));break;}case'placeholderTextColor':{var _block=createDeclarationBlock({color:value,opacity:1});rules.push(selector+"::-webkit-input-placeholder"+_block,selector+"::-moz-placeholder"+_block,selector+":-ms-input-placeholder"+_block,selector+"::placeholder"+_block);break;}case'pointerEvents':{var finalValue=value;if(value==='auto'||value==='box-only'){finalValue='auto!important';if(value==='box-only'){var _block3=createDeclarationBlock({pointerEvents:'none'});rules.push(selector+">*"+_block3);}}else if(value==='none'||value==='box-none'){finalValue='none!important';if(value==='box-none'){var _block4=createDeclarationBlock({pointerEvents:'auto'});rules.push(selector+">*"+_block4);}}var _block2=createDeclarationBlock({pointerEvents:finalValue});rules.push(""+selector+_block2);break;}case'scrollbarWidth':{if(value==='none'){rules.push(selector+"::-webkit-scrollbar{display:none}");}var _block5=createDeclarationBlock({scrollbarWidth:value});rules.push(""+selector+_block5);break;}default:{var _createDeclarationBlo;var _block6=createDeclarationBlock((_createDeclarationBlo={},_createDeclarationBlo[property]=value,_createDeclarationBlo));rules.push(""+selector+_block6);break;}}return rules;}function createDeclarationBlock(style){var domStyle=prefixStyles(StyleSheet_createReactDOMStyle(style));var declarationsString=Object.keys(domStyle).map(function(property){var value=domStyle[property];var prop=external_hyphenate_style_name_default()(property);if(Array.isArray(value)){return value.map(function(v){return prop+":"+v;}).join(';');}else{return prop+":"+value;}}).sort().join(';');return"{"+declarationsString+";}";}function createIdentifier(prefix,name,value){var hashedString=vendor_hash(name+stringifyValueWithProperty(value,name));return  false?undefined:prefix+"-"+hashedString;}function createKeyframes(keyframes){var prefixes=['-webkit-',''];var identifier=createIdentifier('r','animation',keyframes);var steps='{'+Object.keys(keyframes).map(function(stepName){var rule=keyframes[stepName];var block=createDeclarationBlock(rule);return""+stepName+block;}).join('')+'}';var rules=prefixes.map(function(prefix){return"@"+prefix+"keyframes "+identifier+steps;});return{identifier:identifier,rules:rules};}function processKeyframesValue(keyframesValue){if(typeof keyframesValue==='number'){throw new Error("Invalid CSS keyframes type: "+typeof keyframesValue);}var animationNames=[];var rules=[];var value=Array.isArray(keyframesValue)?keyframesValue:[keyframesValue];value.forEach(function(keyframes){if(typeof keyframes==='string'){animationNames.push(keyframes);}else{var _createKeyframes=createKeyframes(keyframes),identifier=_createKeyframes.identifier,keyframesRules=_createKeyframes.rules;animationNames.push(identifier);rules.push.apply(rules,keyframesRules);}});return{animationNames:animationNames,rules:rules};}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/initialRules.js
var resets=['html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0);}','body{margin:0;}','button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0;}','input::-webkit-search-cancel-button,input::-webkit-search-decoration,'+'input::-webkit-search-results-button,input::-webkit-search-results-decoration{display:none;}'];/* harmony default export */ var initialRules = (resets);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/modality.js
var focusVisibleAttributeName='data-focusvisible-polyfill';var modality_rule=":focus:not(["+focusVisibleAttributeName+"]){outline: none;}";var modality_modality=function modality(insertRule){insertRule(modality_rule);if(!ExecutionEnvironment_["canUseDOM"]){return;}var hadKeyboardEvent=true;var hadFocusVisibleRecently=false;var hadFocusVisibleRecentlyTimeout=null;var inputTypesWhitelist={text:true,search:true,url:true,tel:true,email:true,password:true,number:true,date:true,month:true,week:true,time:true,datetime:true,'datetime-local':true};function isValidFocusTarget(el){if(el&&el!==document&&el.nodeName!=='HTML'&&el.nodeName!=='BODY'&&'classList'in el&&'contains'in el.classList){return true;}return false;}function focusTriggersKeyboardModality(el){var type=el.type;var tagName=el.tagName;var isReadOnly=el.readOnly;if(tagName==='INPUT'&&inputTypesWhitelist[type]&&!isReadOnly){return true;}if(tagName==='TEXTAREA'&&!isReadOnly){return true;}if(el.isContentEditable){return true;}return false;}function addFocusVisibleAttribute(el){if(el.hasAttribute(focusVisibleAttributeName)){return;}el.setAttribute(focusVisibleAttributeName,true);}function removeFocusVisibleAttribute(el){el.removeAttribute(focusVisibleAttributeName);}function removeAllFocusVisibleAttributes(){var list=document.querySelectorAll("["+focusVisibleAttributeName+"]");for(var i=0;i<list.length;i+=1){removeFocusVisibleAttribute(list[i]);}}function onKeyDown(e){if(e.key!=='Tab'&&(e.metaKey||e.altKey||e.ctrlKey||e.shiftKey)){return;}if(isValidFocusTarget(document.activeElement)){addFocusVisibleAttribute(document.activeElement);}hadKeyboardEvent=true;}function onPointerDown(e){if(hadKeyboardEvent===true){removeAllFocusVisibleAttributes();}hadKeyboardEvent=false;}function onFocus(e){if(!isValidFocusTarget(e.target)){return;}if(hadKeyboardEvent||focusTriggersKeyboardModality(e.target)){addFocusVisibleAttribute(e.target);}}function onBlur(e){if(!isValidFocusTarget(e.target)){return;}if(e.target.hasAttribute(focusVisibleAttributeName)){hadFocusVisibleRecently=true;window.clearTimeout(hadFocusVisibleRecentlyTimeout);hadFocusVisibleRecentlyTimeout=window.setTimeout(function(){hadFocusVisibleRecently=false;window.clearTimeout(hadFocusVisibleRecentlyTimeout);},100);removeFocusVisibleAttribute(e.target);}}function onVisibilityChange(e){if(document.visibilityState==='hidden'){if(hadFocusVisibleRecently){hadKeyboardEvent=true;}addInitialPointerMoveListeners();}}function addInitialPointerMoveListeners(){document.addEventListener('mousemove',onInitialPointerMove);document.addEventListener('mousedown',onInitialPointerMove);document.addEventListener('mouseup',onInitialPointerMove);document.addEventListener('pointermove',onInitialPointerMove);document.addEventListener('pointerdown',onInitialPointerMove);document.addEventListener('pointerup',onInitialPointerMove);document.addEventListener('touchmove',onInitialPointerMove);document.addEventListener('touchstart',onInitialPointerMove);document.addEventListener('touchend',onInitialPointerMove);}function removeInitialPointerMoveListeners(){document.removeEventListener('mousemove',onInitialPointerMove);document.removeEventListener('mousedown',onInitialPointerMove);document.removeEventListener('mouseup',onInitialPointerMove);document.removeEventListener('pointermove',onInitialPointerMove);document.removeEventListener('pointerdown',onInitialPointerMove);document.removeEventListener('pointerup',onInitialPointerMove);document.removeEventListener('touchmove',onInitialPointerMove);document.removeEventListener('touchstart',onInitialPointerMove);document.removeEventListener('touchend',onInitialPointerMove);}function onInitialPointerMove(e){if(e.target.nodeName==='HTML'){return;}hadKeyboardEvent=false;removeInitialPointerMoveListeners();}document.addEventListener('keydown',onKeyDown,true);document.addEventListener('mousedown',onPointerDown,true);document.addEventListener('pointerdown',onPointerDown,true);document.addEventListener('touchstart',onPointerDown,true);document.addEventListener('focus',onFocus,true);document.addEventListener('blur',onBlur,true);document.addEventListener('visibilitychange',onVisibilityChange,true);addInitialPointerMoveListeners();};/* harmony default export */ var StyleSheet_modality = (modality_modality);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/createStyleResolver.js
function createStyleResolver(){var inserted,sheet,cache;var resolved={css:{},ltr:{},rtl:{},rtlNoSwap:{}};var init=function init(){inserted={css:{},ltr:{},rtl:{},rtlNoSwap:{}};sheet=createOrderedCSSStyleSheet(createCSSStyleSheet(constants["b" /* STYLE_ELEMENT_ID */]));cache={};StyleSheet_modality(function(rule){return sheet.insert(rule,constants["c" /* STYLE_GROUPS */].modality);});initialRules.forEach(function(rule){sheet.insert(rule,constants["c" /* STYLE_GROUPS */].reset);});};init();function addToCache(className,prop,value){if(!cache[prop]){cache[prop]={};}cache[prop][value]=className;}function getClassName(prop,value){var val=stringifyValueWithProperty(value,prop);return cache[prop]&&cache[prop].hasOwnProperty(val)&&cache[prop][val];}function _injectRegisteredStyle(id){var _I18nManager$getConst=I18nManager["a" /* default */].getConstants(),doLeftAndRightSwapInRTL=_I18nManager$getConst.doLeftAndRightSwapInRTL,isRTL=_I18nManager$getConst.isRTL;var dir=isRTL?doLeftAndRightSwapInRTL?'rtl':'rtlNoSwap':'ltr';if(!inserted[dir][id]){var style=StyleSheet_createCompileableStyle(StyleSheet_i18nStyle(Object(flattenStyle["a" /* default */])(id)));var results=atomic(style);Object.keys(results).forEach(function(key){var _results$key=results[key],identifier=_results$key.identifier,property=_results$key.property,rules=_results$key.rules,value=_results$key.value;addToCache(identifier,property,value);rules.forEach(function(rule){var group=constants["c" /* STYLE_GROUPS */].custom[property]||constants["c" /* STYLE_GROUPS */].atomic;sheet.insert(rule,group);});});inserted[dir][id]=true;}}function resolve(style,classList){var nextClassList=[];var props={};if(!style&&!classList){return props;}if(Array.isArray(classList)){modules_flattenArray(classList).forEach(function(identifier){if(identifier){if(inserted.css[identifier]==null&&resolved.css[identifier]!=null){var item=resolved.css[identifier];item.rules.forEach(function(rule){sheet.insert(rule,item.group);});inserted.css[identifier]=true;}nextClassList.push(identifier);}});}if(typeof style==='number'){_injectRegisteredStyle(style);var key=createCacheKey(style);props=_resolveStyle(style,key);}else if(!Array.isArray(style)){props=_resolveStyle(style);}else{var flatArray=modules_flattenArray(style);var isArrayOfNumbers=true;var cacheKey='';for(var i=0;i<flatArray.length;i++){var id=flatArray[i];if(typeof id!=='number'){isArrayOfNumbers=false;}else{if(isArrayOfNumbers){cacheKey+=id+'-';}_injectRegisteredStyle(id);}}var _key=isArrayOfNumbers?createCacheKey(cacheKey):null;props=_resolveStyle(flatArray,_key);}nextClassList.push.apply(nextClassList,props.classList);var finalProps={className:classListToString(nextClassList),classList:nextClassList};if(props.style){finalProps.style=props.style;}return finalProps;}function _resolveStyle(style,key){var _I18nManager$getConst2=I18nManager["a" /* default */].getConstants(),doLeftAndRightSwapInRTL=_I18nManager$getConst2.doLeftAndRightSwapInRTL,isRTL=_I18nManager$getConst2.isRTL;var dir=isRTL?doLeftAndRightSwapInRTL?'rtl':'rtlNoSwap':'ltr';if(key!=null&&resolved[dir][key]!=null){return resolved[dir][key];}var flatStyle=Object(flattenStyle["a" /* default */])(style);var localizedStyle=StyleSheet_createCompileableStyle(StyleSheet_i18nStyle(flatStyle));var props=Object.keys(localizedStyle).sort().reduce(function(props,styleProp){var value=localizedStyle[styleProp];if(value!=null){var className=getClassName(styleProp,value);if(className){props.classList.push(className);}else{if(styleProp==='animationKeyframes'||styleProp==='placeholderTextColor'||styleProp==='pointerEvents'||styleProp==='scrollbarWidth'){var _atomic;var a=atomic((_atomic={},_atomic[styleProp]=value,_atomic));Object.keys(a).forEach(function(key){var _a$key=a[key],identifier=_a$key.identifier,rules=_a$key.rules;props.classList.push(identifier);rules.forEach(function(rule){sheet.insert(rule,constants["c" /* STYLE_GROUPS */].atomic);});});}else{if(!props.style){props.style={};}props.style[styleProp]=value;}}}return props;},{classList:[]});if(props.style){props.style=inline(props.style);}if(key!=null){resolved[dir][key]=props;}return props;}return{getStyleSheet:function getStyleSheet(){var textContent=sheet.getTextContent();if(!ExecutionEnvironment_["canUseDOM"]){init();}return{id:constants["b" /* STYLE_ELEMENT_ID */],textContent:textContent};},createCSS:function createCSS(rules,group){var result={};Object.keys(rules).forEach(function(name){var style=rules[name];var compiled=classic(style,name);Object.keys(compiled).forEach(function(key){var _compiled$key=compiled[key],identifier=_compiled$key.identifier,rules=_compiled$key.rules;resolved.css[identifier]={group:group||constants["c" /* STYLE_GROUPS */].classic,rules:rules};result[name]=identifier;});});return result;},resolve:resolve,get sheet(){return sheet;}};}var createCacheKey=function createCacheKey(id){var prefix='rn';return prefix+"-"+id;};var classListToString=function classListToString(list){return list.join(' ').trim();};
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/styleResolver.js
var styleResolver=createStyleResolver();/* harmony default export */ var StyleSheet_styleResolver = __webpack_exports__["a"] = (styleResolver);

/***/ }),

/***/ "UJjI":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/constants.js");

/***/ }),

/***/ "UlJF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _createClass; });
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

/***/ }),

/***/ "VDXt":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var _defineProperty=__webpack_require__("oI91");var _regeneratorRuntime=__webpack_require__("vJKn");var _classCallCheck=__webpack_require__("/GRZ");var _createClass=__webpack_require__("i2R6");var _inherits=__webpack_require__("48fX");var _possibleConstructorReturn=__webpack_require__("tCBg");var _getPrototypeOf=__webpack_require__("T0f4");var _extends=__webpack_require__("1ccW");var _objectWithoutProperties=__webpack_require__("CPM0");var _toConsumableArray=__webpack_require__("mPvQ");var _excluded=["strategy"];function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);if(enumerableOnly)symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;});keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=arguments[i]!=null?arguments[i]:{};if(i%2){ownKeys(Object(source),true).forEach(function(key){_defineProperty(target,key,source[key]);});}else if(Object.getOwnPropertyDescriptors){Object.defineProperties(target,Object.getOwnPropertyDescriptors(source));}else{ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}}return target;}function _createSuper(Derived){var hasNativeReflectConstruct=_isNativeReflectConstruct();return function _createSuperInternal(){var Super=_getPrototypeOf(Derived),result;if(hasNativeReflectConstruct){var NewTarget=_getPrototypeOf(this).constructor;result=Reflect.construct(Super,arguments,NewTarget);}else{result=Super.apply(this,arguments);}return _possibleConstructorReturn(this,result);};}function _isNativeReflectConstruct(){if(typeof Reflect==="undefined"||!Reflect.construct)return false;if(Reflect.construct.sham)return false;if(typeof Proxy==="function")return true;try{Date.prototype.toString.call(Reflect.construct(Date,[],function(){}));return true;}catch(e){return false;}}exports.__esModule=true;exports.Html=Html;exports.Main=Main;exports.NextScript=exports.Head=exports.default=void 0;var _propTypes=_interopRequireDefault(__webpack_require__("lgD3"));var _react=_interopRequireWildcard(__webpack_require__("cDcd"));var _server=_interopRequireDefault(__webpack_require__("2+G7"));var _constants=__webpack_require__("UJjI");var _documentContext=__webpack_require__("nMq/");var _utils=__webpack_require__("kYf9");exports.DocumentContext=_utils.DocumentContext;exports.DocumentInitialProps=_utils.DocumentInitialProps;exports.DocumentProps=_utils.DocumentProps;var _getPageFiles=__webpack_require__("vHio");var _utils2=__webpack_require__("ri/Y");var _htmlescape=__webpack_require__("AXZJ");var _experimentalScript=_interopRequireDefault(__webpack_require__("24z6"));function _getRequireWildcardCache(){if(typeof WeakMap!=="function")return null;var cache=new WeakMap();_getRequireWildcardCache=function _getRequireWildcardCache(){return cache;};return cache;}function _interopRequireWildcard(obj){if(obj&&obj.__esModule){return obj;}if(obj===null||typeof obj!=="object"&&typeof obj!=="function"){return{default:obj};}var cache=_getRequireWildcardCache();if(cache&&cache.has(obj)){return cache.get(obj);}var newObj={};var hasPropertyDescriptor=Object.defineProperty&&Object.getOwnPropertyDescriptor;for(var key in obj){if(Object.prototype.hasOwnProperty.call(obj,key)){var desc=hasPropertyDescriptor?Object.getOwnPropertyDescriptor(obj,key):null;if(desc&&(desc.get||desc.set)){Object.defineProperty(newObj,key,desc);}else{newObj[key]=obj[key];}}}newObj.default=obj;if(cache){cache.set(obj,newObj);}return newObj;}function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}function getDocumentFiles(buildManifest,pathname,inAmpMode){var sharedFiles=(0,_getPageFiles.getPageFiles)(buildManifest,'/_app');var pageFiles=inAmpMode?[]:(0,_getPageFiles.getPageFiles)(buildManifest,pathname);return{sharedFiles:sharedFiles,pageFiles:pageFiles,allFiles:_toConsumableArray(new Set([].concat(_toConsumableArray(sharedFiles),_toConsumableArray(pageFiles))))};}function _getPolyfillScripts(context,props){var assetPrefix=context.assetPrefix,buildManifest=context.buildManifest,devOnlyCacheBusterQueryString=context.devOnlyCacheBusterQueryString,disableOptimizedLoading=context.disableOptimizedLoading;return buildManifest.polyfillFiles.filter(function(polyfill){return polyfill.endsWith('.js')&&!polyfill.endsWith('.module.js');}).map(function(polyfill){return _react.default.createElement("script",{key:polyfill,defer:!disableOptimizedLoading,nonce:props.nonce,crossOrigin:props.crossOrigin||undefined,noModule:true,src:assetPrefix+"/_next/"+polyfill+devOnlyCacheBusterQueryString});});}function _getPreNextScripts(context,props){var scriptLoader=context.scriptLoader,disableOptimizedLoading=context.disableOptimizedLoading;return(scriptLoader.beforeInteractive||[]).map(function(file){var strategy=file.strategy,scriptProps=_objectWithoutProperties(file,_excluded);return _react.default.createElement("script",_extends({},scriptProps,{defer:!disableOptimizedLoading,nonce:props.nonce,crossOrigin:props.crossOrigin||undefined}));});}function _getDynamicChunks(context,props,files){var dynamicImports=context.dynamicImports,assetPrefix=context.assetPrefix,isDevelopment=context.isDevelopment,devOnlyCacheBusterQueryString=context.devOnlyCacheBusterQueryString,disableOptimizedLoading=context.disableOptimizedLoading;return dynamicImports.map(function(file){if(!file.endsWith('.js')||files.allFiles.includes(file))return null;return _react.default.createElement("script",{async:!isDevelopment&&disableOptimizedLoading,defer:!disableOptimizedLoading,key:file,src:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,nonce:props.nonce,crossOrigin:props.crossOrigin||undefined});});}function _getScripts(context,props,files){var _buildManifest$lowPri;var assetPrefix=context.assetPrefix,buildManifest=context.buildManifest,isDevelopment=context.isDevelopment,devOnlyCacheBusterQueryString=context.devOnlyCacheBusterQueryString,disableOptimizedLoading=context.disableOptimizedLoading;var normalScripts=files.allFiles.filter(function(file){return file.endsWith('.js');});var lowPriorityScripts=(_buildManifest$lowPri=buildManifest.lowPriorityFiles)==null?void 0:_buildManifest$lowPri.filter(function(file){return file.endsWith('.js');});return[].concat(_toConsumableArray(normalScripts),_toConsumableArray(lowPriorityScripts)).map(function(file){return _react.default.createElement("script",{key:file,src:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,nonce:props.nonce,async:!isDevelopment&&disableOptimizedLoading,defer:!disableOptimizedLoading,crossOrigin:props.crossOrigin||undefined});});}var Document=function(_react$Component){_inherits(Document,_react$Component);var _super=_createSuper(Document);function Document(){_classCallCheck(this,Document);return _super.apply(this,arguments);}_createClass(Document,[{key:"render",value:function render(){return _react.default.createElement(Html,null,_react.default.createElement(Head,null),_react.default.createElement("body",null,_react.default.createElement(Main,null),_react.default.createElement(NextScript,null)));}}],[{key:"getInitialProps",value:function getInitialProps(ctx){var enhanceApp,_await$ctx$renderPage,html,head,styles;return _regeneratorRuntime.async(function getInitialProps$(_context){while(1){switch(_context.prev=_context.next){case 0:enhanceApp=function enhanceApp(App){return function(props){return _react.default.createElement(App,props);};};_context.next=3;return _regeneratorRuntime.awrap(ctx.renderPage({enhanceApp:enhanceApp}));case 3:_await$ctx$renderPage=_context.sent;html=_await$ctx$renderPage.html;head=_await$ctx$renderPage.head;styles=_toConsumableArray((0,_server.default)());return _context.abrupt("return",{html:html,head:head,styles:styles});case 8:case"end":return _context.stop();}}},null,null,null,Promise);}},{key:"renderDocument",value:function renderDocument(DocumentComponent,props){return _react.default.createElement(_documentContext.DocumentContext.Provider,{value:props},_react.default.createElement(DocumentComponent,props));}}]);return Document;}(_react.Component);exports.default=Document;function Html(props){var _ref=(0,_react.useContext)(_documentContext.DocumentContext),inAmpMode=_ref.inAmpMode,docComponentsRendered=_ref.docComponentsRendered,locale=_ref.locale;docComponentsRendered.Html=true;return _react.default.createElement("html",_extends({},props,{lang:props.lang||locale||undefined,amp:inAmpMode?'':undefined,"data-ampdevmode":inAmpMode&&false?'':undefined}));}var Head=function(_react$Component2){_inherits(Head,_react$Component2);var _super2=_createSuper(Head);function Head(){var _this;_classCallCheck(this,Head);for(var _len=arguments.length,args=new Array(_len),_key=0;_key<_len;_key++){args[_key]=arguments[_key];}_this=_super2.call.apply(_super2,[this].concat(args));_this.context=void 0;return _this;}_createClass(Head,[{key:"getCssLinks",value:function getCssLinks(files){var _this2=this;var _this$context=this.context,assetPrefix=_this$context.assetPrefix,devOnlyCacheBusterQueryString=_this$context.devOnlyCacheBusterQueryString,dynamicImports=_this$context.dynamicImports;var cssFiles=files.allFiles.filter(function(f){return f.endsWith('.css');});var sharedFiles=new Set(files.sharedFiles);var unmangedFiles=new Set([]);var dynamicCssFiles=Array.from(new Set(dynamicImports.filter(function(file){return file.endsWith('.css');})));if(dynamicCssFiles.length){var existing=new Set(cssFiles);dynamicCssFiles=dynamicCssFiles.filter(function(f){return!(existing.has(f)||sharedFiles.has(f));});unmangedFiles=new Set(dynamicCssFiles);cssFiles.push.apply(cssFiles,_toConsumableArray(dynamicCssFiles));}var cssLinkElements=[];cssFiles.forEach(function(file){var isSharedFile=sharedFiles.has(file);if(true){cssLinkElements.push(_react.default.createElement("link",{key:file+"-preload",nonce:_this2.props.nonce,rel:"preload",href:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,as:"style",crossOrigin:_this2.props.crossOrigin||undefined}));}var isUnmanagedFile=unmangedFiles.has(file);cssLinkElements.push(_react.default.createElement("link",{key:file,nonce:_this2.props.nonce,rel:"stylesheet",href:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,crossOrigin:_this2.props.crossOrigin||undefined,"data-n-g":isUnmanagedFile?undefined:isSharedFile?'':undefined,"data-n-p":isUnmanagedFile?undefined:isSharedFile?undefined:''}));});if(true){cssLinkElements=this.makeStylesheetInert(cssLinkElements);}return cssLinkElements.length===0?null:cssLinkElements;}},{key:"getPreloadDynamicChunks",value:function getPreloadDynamicChunks(){var _this3=this;var _this$context2=this.context,dynamicImports=_this$context2.dynamicImports,assetPrefix=_this$context2.assetPrefix,devOnlyCacheBusterQueryString=_this$context2.devOnlyCacheBusterQueryString;return dynamicImports.map(function(file){if(!file.endsWith('.js')){return null;}return _react.default.createElement("link",{rel:"preload",key:file,href:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,as:"script",nonce:_this3.props.nonce,crossOrigin:_this3.props.crossOrigin||undefined});}).filter(Boolean);}},{key:"getPreloadMainLinks",value:function getPreloadMainLinks(files){var _this4=this;var _this$context3=this.context,assetPrefix=_this$context3.assetPrefix,devOnlyCacheBusterQueryString=_this$context3.devOnlyCacheBusterQueryString,scriptLoader=_this$context3.scriptLoader;var preloadFiles=files.allFiles.filter(function(file){return file.endsWith('.js');});return[].concat(_toConsumableArray((scriptLoader.beforeInteractive||[]).map(function(file){return _react.default.createElement("link",{key:file.src,nonce:_this4.props.nonce,rel:"preload",href:file.src,as:"script",crossOrigin:_this4.props.crossOrigin||undefined});})),_toConsumableArray(preloadFiles.map(function(file){return _react.default.createElement("link",{key:file,nonce:_this4.props.nonce,rel:"preload",href:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,as:"script",crossOrigin:_this4.props.crossOrigin||undefined});})));}},{key:"getDynamicChunks",value:function getDynamicChunks(files){return _getDynamicChunks(this.context,this.props,files);}},{key:"getPreNextScripts",value:function getPreNextScripts(){return _getPreNextScripts(this.context,this.props);}},{key:"getScripts",value:function getScripts(files){return _getScripts(this.context,this.props,files);}},{key:"getPolyfillScripts",value:function getPolyfillScripts(){return _getPolyfillScripts(this.context,this.props);}},{key:"handleDocumentScriptLoaderItems",value:function handleDocumentScriptLoaderItems(children){var scriptLoader=this.context.scriptLoader;var scriptLoaderItems=[];var filteredChildren=[];_react.default.Children.forEach(children,function(child){if(child.type===_experimentalScript.default){if(child.props.strategy==='beforeInteractive'){scriptLoader.beforeInteractive=(scriptLoader.beforeInteractive||[]).concat([_objectSpread({},child.props)]);return;}else if(['lazyOnload','afterInteractive'].includes(child.props.strategy)){scriptLoaderItems.push(child.props);return;}}filteredChildren.push(child);});this.context.__NEXT_DATA__.scriptLoader=scriptLoaderItems;return filteredChildren;}},{key:"makeStylesheetInert",value:function makeStylesheetInert(node){var _this5=this;return _react.default.Children.map(node,function(c){if(c.type==='link'&&c.props['href']&&_constants.OPTIMIZED_FONT_PROVIDERS.some(function(url){return c.props['href'].startsWith(url);})){var newProps=_objectSpread({},c.props||{});newProps['data-href']=newProps['href'];newProps['href']=undefined;return _react.default.cloneElement(c,newProps);}else if(c.props&&c.props['children']){c.props['children']=_this5.makeStylesheetInert(c.props['children']);}return c;});}},{key:"render",value:function render(){var _react$default;var _this$props$nonce,_this$props$nonce2;var _this$context4=this.context,styles=_this$context4.styles,ampPath=_this$context4.ampPath,inAmpMode=_this$context4.inAmpMode,hybridAmp=_this$context4.hybridAmp,canonicalBase=_this$context4.canonicalBase,__NEXT_DATA__=_this$context4.__NEXT_DATA__,dangerousAsPath=_this$context4.dangerousAsPath,headTags=_this$context4.headTags,unstable_runtimeJS=_this$context4.unstable_runtimeJS,unstable_JsPreload=_this$context4.unstable_JsPreload,disableOptimizedLoading=_this$context4.disableOptimizedLoading;var disableRuntimeJS=unstable_runtimeJS===false;var disableJsPreload=unstable_JsPreload===false||!disableOptimizedLoading;this.context.docComponentsRendered.Head=true;var head=this.context.head;var cssPreloads=[];var otherHeadElements=[];if(head){head.forEach(function(c){if(c&&c.type==='link'&&c.props['rel']==='preload'&&c.props['as']==='style'){cssPreloads.push(c);}else{c&&otherHeadElements.push(c);}});head=cssPreloads.concat(otherHeadElements);}var children=_react.default.Children.toArray(this.props.children).filter(Boolean);if(false){}if( true&&!inAmpMode){children=this.makeStylesheetInert(children);}if(false){}var hasAmphtmlRel=false;var hasCanonicalRel=false;head=_react.default.Children.map(head||[],function(child){if(!child)return child;var type=child.type,props=child.props;if(inAmpMode){var badProp='';if(type==='meta'&&props.name==='viewport'){badProp='name="viewport"';}else if(type==='link'&&props.rel==='canonical'){hasCanonicalRel=true;}else if(type==='script'){if(props.src&&props.src.indexOf('ampproject')<-1||props.dangerouslySetInnerHTML&&(!props.type||props.type==='text/javascript')){badProp='<script';Object.keys(props).forEach(function(prop){badProp+=" "+prop+"=\""+props[prop]+"\"";});badProp+='/>';}}if(badProp){console.warn("Found conflicting amp tag \""+child.type+"\" with conflicting prop "+badProp+" in "+__NEXT_DATA__.page+". https://nextjs.org/docs/messages/conflicting-amp-tag");return null;}}else{if(type==='link'&&props.rel==='amphtml'){hasAmphtmlRel=true;}}return child;});var curStyles=Array.isArray(styles)?styles:[];if(inAmpMode&&styles&&styles.props&&Array.isArray(styles.props.children)){var hasStyles=function hasStyles(el){var _el$props,_el$props$dangerously;return el==null?void 0:(_el$props=el.props)==null?void 0:(_el$props$dangerously=_el$props.dangerouslySetInnerHTML)==null?void 0:_el$props$dangerously.__html;};styles.props.children.forEach(function(child){if(Array.isArray(child)){child.forEach(function(el){return hasStyles(el)&&curStyles.push(el);});}else if(hasStyles(child)){curStyles.push(child);}});}var files=getDocumentFiles(this.context.buildManifest,this.context.__NEXT_DATA__.page,inAmpMode);return _react.default.createElement("head",this.props,this.context.isDevelopment&&_react.default.createElement(_react.default.Fragment,null,_react.default.createElement("style",{"data-next-hide-fouc":true,"data-ampdevmode":inAmpMode?'true':undefined,dangerouslySetInnerHTML:{__html:"body{display:none}"}}),_react.default.createElement("noscript",{"data-next-hide-fouc":true,"data-ampdevmode":inAmpMode?'true':undefined},_react.default.createElement("style",{dangerouslySetInnerHTML:{__html:"body{display:block}"}}))),children,head,_react.default.createElement("meta",{name:"next-head-count",content:_react.default.Children.count(head||[]).toString()}),inAmpMode&&_react.default.createElement(_react.default.Fragment,null,_react.default.createElement("meta",{name:"viewport",content:"width=device-width,minimum-scale=1,initial-scale=1"}),!hasCanonicalRel&&_react.default.createElement("link",{rel:"canonical",href:canonicalBase+(0,_utils2.cleanAmpPath)(dangerousAsPath)}),_react.default.createElement("link",{rel:"preload",as:"script",href:"https://cdn.ampproject.org/v0.js"}),styles&&_react.default.createElement("style",{"amp-custom":"",dangerouslySetInnerHTML:{__html:curStyles.map(function(style){return style.props.dangerouslySetInnerHTML.__html;}).join('').replace(/\/\*# sourceMappingURL=.*\*\//g,'').replace(/\/\*@ sourceURL=.*?\*\//g,'')}}),_react.default.createElement("style",{"amp-boilerplate":"",dangerouslySetInnerHTML:{__html:"body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}"}}),_react.default.createElement("noscript",null,_react.default.createElement("style",{"amp-boilerplate":"",dangerouslySetInnerHTML:{__html:"body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}"}})),_react.default.createElement("script",{async:true,src:"https://cdn.ampproject.org/v0.js"})),!inAmpMode&&_react.default.createElement(_react.default.Fragment,null,!hasAmphtmlRel&&hybridAmp&&_react.default.createElement("link",{rel:"amphtml",href:canonicalBase+getAmpPath(ampPath,dangerousAsPath)}), true&&this.getCssLinks(files), true&&_react.default.createElement("noscript",{"data-n-css":(_this$props$nonce=this.props.nonce)!=null?_this$props$nonce:''}),!disableRuntimeJS&&!disableJsPreload&&this.getPreloadDynamicChunks(),!disableRuntimeJS&&!disableJsPreload&&this.getPreloadMainLinks(files),!disableOptimizedLoading&&!disableRuntimeJS&&this.getPolyfillScripts(),!disableOptimizedLoading&&!disableRuntimeJS&&this.getPreNextScripts(),!disableOptimizedLoading&&!disableRuntimeJS&&this.getDynamicChunks(files),!disableOptimizedLoading&&!disableRuntimeJS&&this.getScripts(files), false&&false, false&&false,this.context.isDevelopment&&_react.default.createElement("noscript",{id:"__next_css__DO_NOT_USE__"}),styles||null),(_react$default=_react.default).createElement.apply(_react$default,[_react.default.Fragment,{}].concat(_toConsumableArray(headTags||[]))));}}]);return Head;}(_react.Component);exports.Head=Head;Head.contextType=_documentContext.DocumentContext;Head.propTypes={nonce:_propTypes.default.string,crossOrigin:_propTypes.default.string};function Main(){var _ref2=(0,_react.useContext)(_documentContext.DocumentContext),inAmpMode=_ref2.inAmpMode,html=_ref2.html,docComponentsRendered=_ref2.docComponentsRendered;docComponentsRendered.Main=true;if(inAmpMode)return _react.default.createElement(_react.default.Fragment,null,_constants.AMP_RENDER_TARGET);return _react.default.createElement("div",{id:"__next",dangerouslySetInnerHTML:{__html:html}});}var NextScript=function(_react$Component3){_inherits(NextScript,_react$Component3);var _super3=_createSuper(NextScript);function NextScript(){var _this6;_classCallCheck(this,NextScript);for(var _len2=arguments.length,args=new Array(_len2),_key2=0;_key2<_len2;_key2++){args[_key2]=arguments[_key2];}_this6=_super3.call.apply(_super3,[this].concat(args));_this6.context=void 0;return _this6;}_createClass(NextScript,[{key:"getDynamicChunks",value:function getDynamicChunks(files){return _getDynamicChunks(this.context,this.props,files);}},{key:"getPreNextScripts",value:function getPreNextScripts(){return _getPreNextScripts(this.context,this.props);}},{key:"getScripts",value:function getScripts(files){return _getScripts(this.context,this.props,files);}},{key:"getPolyfillScripts",value:function getPolyfillScripts(){return _getPolyfillScripts(this.context,this.props);}},{key:"render",value:function render(){var _this7=this;var _this$context5=this.context,assetPrefix=_this$context5.assetPrefix,inAmpMode=_this$context5.inAmpMode,buildManifest=_this$context5.buildManifest,unstable_runtimeJS=_this$context5.unstable_runtimeJS,docComponentsRendered=_this$context5.docComponentsRendered,devOnlyCacheBusterQueryString=_this$context5.devOnlyCacheBusterQueryString,disableOptimizedLoading=_this$context5.disableOptimizedLoading;var disableRuntimeJS=unstable_runtimeJS===false;docComponentsRendered.NextScript=true;if(inAmpMode){if(true){return null;}var ampDevFiles=[].concat(_toConsumableArray(buildManifest.devFiles),_toConsumableArray(buildManifest.polyfillFiles),_toConsumableArray(buildManifest.ampDevFiles));return _react.default.createElement(_react.default.Fragment,null,disableRuntimeJS?null:_react.default.createElement("script",{id:"__NEXT_DATA__",type:"application/json",nonce:this.props.nonce,crossOrigin:this.props.crossOrigin||undefined,dangerouslySetInnerHTML:{__html:NextScript.getInlineScriptSource(this.context)},"data-ampdevmode":true}),ampDevFiles.map(function(file){return _react.default.createElement("script",{key:file,src:assetPrefix+"/_next/"+file+devOnlyCacheBusterQueryString,nonce:_this7.props.nonce,crossOrigin:_this7.props.crossOrigin||undefined,"data-ampdevmode":true});}));}if(false){}var files=getDocumentFiles(this.context.buildManifest,this.context.__NEXT_DATA__.page,inAmpMode);return _react.default.createElement(_react.default.Fragment,null,!disableRuntimeJS&&buildManifest.devFiles?buildManifest.devFiles.map(function(file){return _react.default.createElement("script",{key:file,src:assetPrefix+"/_next/"+encodeURI(file)+devOnlyCacheBusterQueryString,nonce:_this7.props.nonce,crossOrigin:_this7.props.crossOrigin||undefined});}):null,disableRuntimeJS?null:_react.default.createElement("script",{id:"__NEXT_DATA__",type:"application/json",nonce:this.props.nonce,crossOrigin:this.props.crossOrigin||undefined,dangerouslySetInnerHTML:{__html:NextScript.getInlineScriptSource(this.context)}}),disableOptimizedLoading&&!disableRuntimeJS&&this.getPolyfillScripts(),disableOptimizedLoading&&!disableRuntimeJS&&this.getPreNextScripts(),disableOptimizedLoading&&!disableRuntimeJS&&this.getDynamicChunks(files),disableOptimizedLoading&&!disableRuntimeJS&&this.getScripts(files));}}],[{key:"getInlineScriptSource",value:function getInlineScriptSource(documentProps){var __NEXT_DATA__=documentProps.__NEXT_DATA__;try{var data=JSON.stringify(__NEXT_DATA__);return(0,_htmlescape.htmlEscapeJsonString)(data);}catch(err){if(err.message.indexOf('circular structure')){throw new Error("Circular structure in \"getInitialProps\" result of page \""+__NEXT_DATA__.page+"\". https://nextjs.org/docs/messages/circular-structure");}throw err;}}}]);return NextScript;}(_react.Component);exports.NextScript=NextScript;NextScript.contextType=_documentContext.DocumentContext;NextScript.propTypes={nonce:_propTypes.default.string,crossOrigin:_propTypes.default.string};NextScript.safariNomoduleFix='!function(){var e=document,t=e.createElement("script");if(!("noModule"in t)&&"onbeforeload"in t){var n=!1;e.addEventListener("beforeload",function(e){if(e.target===t)n=!0;else if(!e.target.hasAttribute("nomodule")||!n)return;e.preventDefault()},!0),t.type="module",t.src=".",e.head.appendChild(t),t.remove()}}();';function getAmpPath(ampPath,asPath){return ampPath||""+asPath+(asPath.includes('?')?'&':'?')+"amp=1";}

/***/ }),

/***/ "VIvw":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ _possibleConstructorReturn; });

// CONCATENATED MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/typeof.js
function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}
// CONCATENATED MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js
function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}
// CONCATENATED MODULE: ./node_modules/next/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js


function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

/***/ }),

/***/ "VKdU":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mergeRefs; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function mergeRefs(){for(var _len=arguments.length,args=new Array(_len),_key=0;_key<_len;_key++){args[_key]=arguments[_key];}return function forwardRef(node){args.forEach(function(ref){if(ref==null){return;}if(typeof ref==='function'){ref(node);return;}if(typeof ref==='object'){ref.current=node;return;}console.error("mergeRefs cannot handle Refs of type boolean, number or string, received ref "+String(ref));});};}

/***/ }),

/***/ "XFth":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReactNativePropRegistry; });
var emptyObject={};var objects={};var prefix='r';var uniqueID=1;var createKey=function createKey(id){return prefix+"-"+id;};var ReactNativePropRegistry=function(){function ReactNativePropRegistry(){}ReactNativePropRegistry.register=function register(object){var id=uniqueID++;if(false){}var key=createKey(id);objects[key]=object;return id;};ReactNativePropRegistry.getByID=function getByID(id){if(!id){return emptyObject;}var key=createKey(id);var object=objects[key];if(!object){console.warn('Invalid style with id `'+id+'`. Skipping ...');return emptyObject;}return object;};return ReactNativePropRegistry;}();

/***/ }),

/***/ "Y8yu":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/head-manager-context.js");

/***/ }),

/***/ "Yl5I":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("L1DD");
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
var useLayoutEffectImpl=fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__["canUseDOM"]?react__WEBPACK_IMPORTED_MODULE_1__["useLayoutEffect"]:react__WEBPACK_IMPORTED_MODULE_1__["useEffect"];/* harmony default export */ __webpack_exports__["a"] = (useLayoutEffectImpl);

/***/ }),

/***/ "aD8d":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/position");

/***/ }),

/***/ "bcFB":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/imageSet");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cdkR":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/isWebColor/index.js
var isWebColor=function isWebColor(color){return color==='currentcolor'||color==='currentColor'||color==='inherit'||color.indexOf('var(')===0;};/* harmony default export */ var modules_isWebColor = (isWebColor);
// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/processColor/index.js
var processColor = __webpack_require__("+mtM");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/modules/normalizeColor/index.js
var normalizeColor_normalizeColor=function normalizeColor(color,opacity){if(opacity===void 0){opacity=1;}if(color==null)return;if(typeof color==='string'&&modules_isWebColor(color)){return color;}var colorInt=Object(processColor["a" /* default */])(color);if(colorInt!=null){var r=colorInt>>16&255;var g=colorInt>>8&255;var b=colorInt&255;var a=(colorInt>>24&255)/255;var alpha=(a*opacity).toFixed(2);return"rgba("+r+","+g+","+b+","+alpha+")";}};/* harmony default export */ var modules_normalizeColor = __webpack_exports__["a"] = (normalizeColor_normalizeColor);

/***/ }),

/***/ "ckSu":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "fbjs/lib/ExecutionEnvironment"
var ExecutionEnvironment_ = __webpack_require__("L1DD");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/ReactNativePropRegistry.js
var ReactNativePropRegistry = __webpack_require__("XFth");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/flattenStyle.js
var flattenStyle = __webpack_require__("Iu5r");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/StyleSheet.js
var absoluteFillObject={position:'absolute',left:0,right:0,top:0,bottom:0};var absoluteFill=ReactNativePropRegistry["a" /* default */].register(absoluteFillObject);var StyleSheet={absoluteFill:absoluteFill,absoluteFillObject:absoluteFillObject,compose:function compose(style1,style2){if(false){ var readableStyles, len; }if(style1&&style2){return[style1,style2];}else{return style1||style2;}},create:function create(styles){var result={};Object.keys(styles).forEach(function(key){if(false){}var id=styles[key]&&ReactNativePropRegistry["a" /* default */].register(styles[key]);result[key]=id;});return result;},flatten:flattenStyle["a" /* default */],hairlineWidth:1};/* harmony default export */ var StyleSheet_StyleSheet = (StyleSheet);
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/index.js
if(ExecutionEnvironment_["canUseDOM"]&&window.__REACT_DEVTOOLS_GLOBAL_HOOK__){window.__REACT_DEVTOOLS_GLOBAL_HOOK__.resolveRNStyle=StyleSheet_StyleSheet.flatten;}/* harmony default export */ var exports_StyleSheet = __webpack_exports__["a"] = (StyleSheet_StyleSheet);

/***/ }),

/***/ "cpVT":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _defineProperty; });
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "cqcS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _modules_normalizeColor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cdkR");
/* harmony import */ var _normalizeValueWithProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("A6cL");
var defaultOffset={height:0,width:0};var resolveShadowValue=function resolveShadowValue(style){var shadowColor=style.shadowColor,shadowOffset=style.shadowOffset,shadowOpacity=style.shadowOpacity,shadowRadius=style.shadowRadius;var _ref=shadowOffset||defaultOffset,height=_ref.height,width=_ref.width;var offsetX=Object(_normalizeValueWithProperty__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(width);var offsetY=Object(_normalizeValueWithProperty__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(height);var blurRadius=Object(_normalizeValueWithProperty__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(shadowRadius||0);var color=Object(_modules_normalizeColor__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(shadowColor||'black',shadowOpacity);if(color!=null&&offsetX!=null&&offsetY!=null&&blurRadius!=null){return offsetX+" "+offsetY+" "+blurRadius+" "+color;}};/* harmony default export */ __webpack_exports__["a"] = (resolveShadowValue);

/***/ }),

/***/ "cu1A":
/***/ (function(module, exports) {

module.exports = require("regenerator-runtime");

/***/ }),

/***/ "eJic":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/flexboxIE");

/***/ }),

/***/ "eNWT":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/createPrefixer");

/***/ }),

/***/ "eRNP":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/flex");

/***/ }),

/***/ "eZI+":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/backgroundClip");

/***/ }),

/***/ "ebBd":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _AccessibilityUtil__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("IybW");
/* harmony import */ var _exports_StyleSheet_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("lxpi");
/* harmony import */ var _exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("ckSu");
/* harmony import */ var _exports_StyleSheet_styleResolver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("TgPG");
/* harmony import */ var _exports_StyleSheet_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("yBWl");
function _objectWithoutPropertiesLoose(source,excluded){if(source==null)return{};var target={};var sourceKeys=Object.keys(source);var key,i;for(i=0;i<sourceKeys.length;i++){key=sourceKeys[i];if(excluded.indexOf(key)>=0)continue;target[key]=source[key];}return target;}var emptyObject={};var hasOwnProperty=Object.prototype.hasOwnProperty;var isArray=Array.isArray;var uppercasePattern=/[A-Z]/g;function toHyphenLower(match){return'-'+match.toLowerCase();}function hyphenateString(str){return str.replace(uppercasePattern,toHyphenLower);}function processIDRefList(idRefList){return isArray(idRefList)?idRefList.join(' '):idRefList;}var classes=_exports_StyleSheet_css__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].create({reset:{backgroundColor:'transparent',color:'inherit',font:'inherit',listStyle:'none',margin:0,textAlign:'inherit',textDecoration:'none'},cursor:{cursor:'pointer'}},_exports_StyleSheet_constants__WEBPACK_IMPORTED_MODULE_4__[/* STYLE_GROUPS */ "c"].classicReset);var pointerEventsStyles=_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].create({auto:{pointerEvents:'auto'},'box-none':{pointerEvents:'box-none'},'box-only':{pointerEvents:'box-only'},none:{pointerEvents:'none'}});var createDOMProps=function createDOMProps(elementType,props){if(!props){props=emptyObject;}var _props=props,accessibilityActiveDescendant=_props.accessibilityActiveDescendant,accessibilityAtomic=_props.accessibilityAtomic,accessibilityAutoComplete=_props.accessibilityAutoComplete,accessibilityBusy=_props.accessibilityBusy,accessibilityChecked=_props.accessibilityChecked,accessibilityColumnCount=_props.accessibilityColumnCount,accessibilityColumnIndex=_props.accessibilityColumnIndex,accessibilityColumnSpan=_props.accessibilityColumnSpan,accessibilityControls=_props.accessibilityControls,accessibilityCurrent=_props.accessibilityCurrent,accessibilityDescribedBy=_props.accessibilityDescribedBy,accessibilityDetails=_props.accessibilityDetails,accessibilityDisabled=_props.accessibilityDisabled,accessibilityErrorMessage=_props.accessibilityErrorMessage,accessibilityExpanded=_props.accessibilityExpanded,accessibilityFlowTo=_props.accessibilityFlowTo,accessibilityHasPopup=_props.accessibilityHasPopup,accessibilityHidden=_props.accessibilityHidden,accessibilityInvalid=_props.accessibilityInvalid,accessibilityKeyShortcuts=_props.accessibilityKeyShortcuts,accessibilityLabel=_props.accessibilityLabel,accessibilityLabelledBy=_props.accessibilityLabelledBy,accessibilityLevel=_props.accessibilityLevel,accessibilityLiveRegion=_props.accessibilityLiveRegion,accessibilityModal=_props.accessibilityModal,accessibilityMultiline=_props.accessibilityMultiline,accessibilityMultiSelectable=_props.accessibilityMultiSelectable,accessibilityOrientation=_props.accessibilityOrientation,accessibilityOwns=_props.accessibilityOwns,accessibilityPlaceholder=_props.accessibilityPlaceholder,accessibilityPosInSet=_props.accessibilityPosInSet,accessibilityPressed=_props.accessibilityPressed,accessibilityReadOnly=_props.accessibilityReadOnly,accessibilityRequired=_props.accessibilityRequired,accessibilityRole=_props.accessibilityRole,accessibilityRoleDescription=_props.accessibilityRoleDescription,accessibilityRowCount=_props.accessibilityRowCount,accessibilityRowIndex=_props.accessibilityRowIndex,accessibilityRowSpan=_props.accessibilityRowSpan,accessibilitySelected=_props.accessibilitySelected,accessibilitySetSize=_props.accessibilitySetSize,accessibilitySort=_props.accessibilitySort,accessibilityValueMax=_props.accessibilityValueMax,accessibilityValueMin=_props.accessibilityValueMin,accessibilityValueNow=_props.accessibilityValueNow,accessibilityValueText=_props.accessibilityValueText,classList=_props.classList,dataSet=_props.dataSet,focusable=_props.focusable,nativeID=_props.nativeID,pointerEvents=_props.pointerEvents,providedStyle=_props.style,testID=_props.testID,accessible=_props.accessible,accessibilityState=_props.accessibilityState,accessibilityValue=_props.accessibilityValue,domProps=_objectWithoutPropertiesLoose(_props,["accessibilityActiveDescendant","accessibilityAtomic","accessibilityAutoComplete","accessibilityBusy","accessibilityChecked","accessibilityColumnCount","accessibilityColumnIndex","accessibilityColumnSpan","accessibilityControls","accessibilityCurrent","accessibilityDescribedBy","accessibilityDetails","accessibilityDisabled","accessibilityErrorMessage","accessibilityExpanded","accessibilityFlowTo","accessibilityHasPopup","accessibilityHidden","accessibilityInvalid","accessibilityKeyShortcuts","accessibilityLabel","accessibilityLabelledBy","accessibilityLevel","accessibilityLiveRegion","accessibilityModal","accessibilityMultiline","accessibilityMultiSelectable","accessibilityOrientation","accessibilityOwns","accessibilityPlaceholder","accessibilityPosInSet","accessibilityPressed","accessibilityReadOnly","accessibilityRequired","accessibilityRole","accessibilityRoleDescription","accessibilityRowCount","accessibilityRowIndex","accessibilityRowSpan","accessibilitySelected","accessibilitySetSize","accessibilitySort","accessibilityValueMax","accessibilityValueMin","accessibilityValueNow","accessibilityValueText","classList","dataSet","focusable","nativeID","pointerEvents","style","testID","accessible","accessibilityState","accessibilityValue"]);var disabled=accessibilityState!=null&&accessibilityState.disabled===true||accessibilityDisabled;var role=_AccessibilityUtil__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].propsToAriaRole(props);if(accessibilityState!=null){for(var prop in accessibilityState){var value=accessibilityState[prop];if(value!=null){if(prop==='disabled'||prop==='hidden'){if(value===true){domProps["aria-"+prop]=value;domProps[prop]=value;}}else{domProps["aria-"+prop]=value;}}}}if(accessibilityValue!=null){for(var _prop in accessibilityValue){var _value=accessibilityValue[_prop];if(_value!=null){domProps["aria-value"+_prop]=_value;}}}if(accessibilityActiveDescendant!=null){domProps['aria-activedescendant']=accessibilityActiveDescendant;}if(accessibilityAtomic!=null){domProps['aria-atomic']=accessibilityAtomic;}if(accessibilityAutoComplete!=null){domProps['aria-autocomplete']=accessibilityAutoComplete;}if(accessibilityBusy!=null){domProps['aria-busy']=accessibilityBusy;}if(accessibilityChecked!=null){domProps['aria-checked']=accessibilityChecked;}if(accessibilityColumnCount!=null){domProps['aria-colcount']=accessibilityColumnCount;}if(accessibilityColumnIndex!=null){domProps['aria-colindex']=accessibilityColumnIndex;}if(accessibilityColumnSpan!=null){domProps['aria-colspan']=accessibilityColumnSpan;}if(accessibilityControls!=null){domProps['aria-controls']=processIDRefList(accessibilityControls);}if(accessibilityCurrent!=null){domProps['aria-current']=accessibilityCurrent;}if(accessibilityDescribedBy!=null){domProps['aria-describedby']=processIDRefList(accessibilityDescribedBy);}if(accessibilityDetails!=null){domProps['aria-details']=accessibilityDetails;}if(disabled===true){domProps['aria-disabled']=true;if(elementType==='button'||elementType==='form'||elementType==='input'||elementType==='select'||elementType==='textarea'){domProps.disabled=true;}}if(accessibilityErrorMessage!=null){domProps['aria-errormessage']=accessibilityErrorMessage;}if(accessibilityExpanded!=null){domProps['aria-expanded']=accessibilityExpanded;}if(accessibilityFlowTo!=null){domProps['aria-flowto']=processIDRefList(accessibilityFlowTo);}if(accessibilityHasPopup!=null){domProps['aria-haspopup']=accessibilityHasPopup;}if(accessibilityHidden===true){domProps['aria-hidden']=accessibilityHidden;}if(accessibilityInvalid!=null){domProps['aria-invalid']=accessibilityInvalid;}if(accessibilityKeyShortcuts!=null&&Array.isArray(accessibilityKeyShortcuts)){domProps['aria-keyshortcuts']=accessibilityKeyShortcuts.join(' ');}if(accessibilityLabel!=null){domProps['aria-label']=accessibilityLabel;}if(accessibilityLabelledBy!=null){domProps['aria-labelledby']=processIDRefList(accessibilityLabelledBy);}if(accessibilityLevel!=null){domProps['aria-level']=accessibilityLevel;}if(accessibilityLiveRegion!=null){domProps['aria-live']=accessibilityLiveRegion==='none'?'off':accessibilityLiveRegion;}if(accessibilityModal!=null){domProps['aria-modal']=accessibilityModal;}if(accessibilityMultiline!=null){domProps['aria-multiline']=accessibilityMultiline;}if(accessibilityMultiSelectable!=null){domProps['aria-multiselectable']=accessibilityMultiSelectable;}if(accessibilityOrientation!=null){domProps['aria-orientation']=accessibilityOrientation;}if(accessibilityOwns!=null){domProps['aria-owns']=processIDRefList(accessibilityOwns);}if(accessibilityPlaceholder!=null){domProps['aria-placeholder']=accessibilityPlaceholder;}if(accessibilityPosInSet!=null){domProps['aria-posinset']=accessibilityPosInSet;}if(accessibilityPressed!=null){domProps['aria-pressed']=accessibilityPressed;}if(accessibilityReadOnly!=null){domProps['aria-readonly']=accessibilityReadOnly;if(elementType==='input'||elementType==='select'||elementType==='textarea'){domProps.readOnly=true;}}if(accessibilityRequired!=null){domProps['aria-required']=accessibilityRequired;if(elementType==='input'||elementType==='select'||elementType==='textarea'){domProps.required=true;}}if(role!=null){domProps['role']=role==='none'?'presentation':role;}if(accessibilityRoleDescription!=null){domProps['aria-roledescription']=accessibilityRoleDescription;}if(accessibilityRowCount!=null){domProps['aria-rowcount']=accessibilityRowCount;}if(accessibilityRowIndex!=null){domProps['aria-rowindex']=accessibilityRowIndex;}if(accessibilityRowSpan!=null){domProps['aria-rowspan']=accessibilityRowSpan;}if(accessibilitySelected!=null){domProps['aria-selected']=accessibilitySelected;}if(accessibilitySetSize!=null){domProps['aria-setsize']=accessibilitySetSize;}if(accessibilitySort!=null){domProps['aria-sort']=accessibilitySort;}if(accessibilityValueMax!=null){domProps['aria-valuemax']=accessibilityValueMax;}if(accessibilityValueMin!=null){domProps['aria-valuemin']=accessibilityValueMin;}if(accessibilityValueNow!=null){domProps['aria-valuenow']=accessibilityValueNow;}if(accessibilityValueText!=null){domProps['aria-valuetext']=accessibilityValueText;}if(dataSet!=null){for(var dataProp in dataSet){if(hasOwnProperty.call(dataSet,dataProp)){var dataName=hyphenateString(dataProp);var dataValue=dataSet[dataProp];if(dataValue!=null){domProps["data-"+dataName]=dataValue;}}}}var _focusable=focusable!=null?focusable:accessible;if(_focusable===false){domProps.tabIndex='-1';}if(elementType==='a'||elementType==='button'||elementType==='input'||elementType==='select'||elementType==='textarea'){if(_focusable===false||accessibilityDisabled===true){domProps.tabIndex='-1';}}else if(role==='button'||role==='checkbox'||role==='link'||role==='radio'||role==='textbox'||role==='switch'){if(_focusable!==false){domProps.tabIndex='0';}}else{if(_focusable===true){domProps.tabIndex='0';}}var reactNativeStyle=_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].compose(pointerEvents&&pointerEventsStyles[pointerEvents],providedStyle);var needsCursor=(role==='button'||role==='link')&&!disabled;var needsReset=elementType==='a'||elementType==='button'||elementType==='li'||elementType==='ul'||role==='heading';var finalClassList=[needsReset&&classes.reset,needsCursor&&classes.cursor,classList];var _styleResolver$resolv=_exports_StyleSheet_styleResolver__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"].resolve(reactNativeStyle,finalClassList),className=_styleResolver$resolv.className,style=_styleResolver$resolv.style;if(className!=null&&className!==''){domProps.className=className;}if(style){domProps.style=style;}if(nativeID!=null){domProps.id=nativeID;}if(testID!=null){domProps['data-testid']=testID;}return domProps;};/* harmony default export */ __webpack_exports__["a"] = (createDOMProps);

/***/ }),

/***/ "faye":
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "gR6W":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return useElementLayout; });
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("L1DD");
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _useLayoutEffect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("Yl5I");
/* harmony import */ var _exports_UIManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("56YO");
var DOM_LAYOUT_HANDLER_NAME='__reactLayoutHandler';var didWarn=!fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__["canUseDOM"];var resizeObserver=null;function getResizeObserver(){if(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_0__["canUseDOM"]&&typeof window.ResizeObserver!=='undefined'){if(resizeObserver==null){resizeObserver=new window.ResizeObserver(function(entries){entries.forEach(function(entry){var node=entry.target;var onLayout=node[DOM_LAYOUT_HANDLER_NAME];if(typeof onLayout==='function'){_exports_UIManager__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].measure(node,function(x,y,width,height,left,top){var event={nativeEvent:{layout:{x:x,y:y,width:width,height:height,left:left,top:top}},timeStamp:Date.now()};Object.defineProperty(event.nativeEvent,'target',{enumerable:true,get:function get(){return entry.target;}});onLayout(event);});}});});}}else if(!didWarn){if(false){}}return resizeObserver;}function useElementLayout(ref,onLayout){var observer=getResizeObserver();Object(_useLayoutEffect__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(function(){var node=ref.current;if(node!=null){node[DOM_LAYOUT_HANDLER_NAME]=onLayout;}},[ref,onLayout]);Object(_useLayoutEffect__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(function(){var node=ref.current;if(node!=null&&observer!=null){if(typeof node[DOM_LAYOUT_HANDLER_NAME]==='function'){observer.observe(node);}else{observer.unobserve(node);}}return function(){if(node!=null&&observer!=null){observer.unobserve(node);}};},[ref,observer]);}

/***/ }),

/***/ "gWRB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return useStable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var UNINITIALIZED=typeof Symbol==='function'&&typeof Symbol()==='symbol'?Symbol():Object.freeze({});function useStable(getInitialValue){var ref=react__WEBPACK_IMPORTED_MODULE_0__["useRef"](UNINITIALIZED);if(ref.current===UNINITIALIZED){ref.current=getInitialValue();}return ref.current;}

/***/ }),

/***/ "gwwY":
/***/ (function(module, exports) {

module.exports = require("hyphenate-style-name");

/***/ }),

/***/ "i2R6":
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),

/***/ "iHvq":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _getPrototypeOf; });
function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

/***/ }),

/***/ "iJv9":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/gradient");

/***/ }),

/***/ "jzUF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _createElement__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("osi3");
/* harmony import */ var _StyleSheet_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("lxpi");
/* harmony import */ var _modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("KCPX");
/* harmony import */ var _modules_pick__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("5Bvt");
/* harmony import */ var _modules_useElementLayout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("gR6W");
/* harmony import */ var _modules_useMergeRefs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("CSmu");
/* harmony import */ var _modules_usePlatformMethods__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("tkvJ");
/* harmony import */ var _modules_useResponderEvents__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("HSq1");
/* harmony import */ var _StyleSheet__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("ckSu");
/* harmony import */ var _Text_TextAncestorContext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("Q3tu");
function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);if(enumerableOnly)symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;});keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=arguments[i]!=null?arguments[i]:{};if(i%2){ownKeys(Object(source),true).forEach(function(key){_defineProperty(target,key,source[key]);});}else if(Object.getOwnPropertyDescriptors){Object.defineProperties(target,Object.getOwnPropertyDescriptors(source));}else{ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}}return target;}function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else{obj[key]=value;}return obj;}var forwardPropsList=_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({},_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* defaultProps */ "c"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* accessibilityProps */ "a"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* clickProps */ "b"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* focusProps */ "d"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* keyboardProps */ "e"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* mouseProps */ "f"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* touchProps */ "h"]),_modules_forwardedProps__WEBPACK_IMPORTED_MODULE_3__[/* styleProps */ "g"]),{},{href:true,lang:true,onScroll:true,onWheel:true,pointerEvents:true});var pickProps=function pickProps(props){return Object(_modules_pick__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"])(props,forwardPropsList);};var View=react__WEBPACK_IMPORTED_MODULE_0__["forwardRef"](function(props,forwardedRef){var hrefAttrs=props.hrefAttrs,onLayout=props.onLayout,onMoveShouldSetResponder=props.onMoveShouldSetResponder,onMoveShouldSetResponderCapture=props.onMoveShouldSetResponderCapture,onResponderEnd=props.onResponderEnd,onResponderGrant=props.onResponderGrant,onResponderMove=props.onResponderMove,onResponderReject=props.onResponderReject,onResponderRelease=props.onResponderRelease,onResponderStart=props.onResponderStart,onResponderTerminate=props.onResponderTerminate,onResponderTerminationRequest=props.onResponderTerminationRequest,onScrollShouldSetResponder=props.onScrollShouldSetResponder,onScrollShouldSetResponderCapture=props.onScrollShouldSetResponderCapture,onSelectionChangeShouldSetResponder=props.onSelectionChangeShouldSetResponder,onSelectionChangeShouldSetResponderCapture=props.onSelectionChangeShouldSetResponderCapture,onStartShouldSetResponder=props.onStartShouldSetResponder,onStartShouldSetResponderCapture=props.onStartShouldSetResponderCapture;if(false){}var hasTextAncestor=react__WEBPACK_IMPORTED_MODULE_0__["useContext"](_Text_TextAncestorContext__WEBPACK_IMPORTED_MODULE_10__[/* default */ "a"]);var hostRef=react__WEBPACK_IMPORTED_MODULE_0__["useRef"](null);Object(_modules_useElementLayout__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"])(hostRef,onLayout);Object(_modules_useResponderEvents__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"])(hostRef,{onMoveShouldSetResponder:onMoveShouldSetResponder,onMoveShouldSetResponderCapture:onMoveShouldSetResponderCapture,onResponderEnd:onResponderEnd,onResponderGrant:onResponderGrant,onResponderMove:onResponderMove,onResponderReject:onResponderReject,onResponderRelease:onResponderRelease,onResponderStart:onResponderStart,onResponderTerminate:onResponderTerminate,onResponderTerminationRequest:onResponderTerminationRequest,onScrollShouldSetResponder:onScrollShouldSetResponder,onScrollShouldSetResponderCapture:onScrollShouldSetResponderCapture,onSelectionChangeShouldSetResponder:onSelectionChangeShouldSetResponder,onSelectionChangeShouldSetResponderCapture:onSelectionChangeShouldSetResponderCapture,onStartShouldSetResponder:onStartShouldSetResponder,onStartShouldSetResponderCapture:onStartShouldSetResponderCapture});var component='div';var style=_StyleSheet__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"].compose(hasTextAncestor&&styles.inline,props.style);var supportedProps=pickProps(props);supportedProps.classList=classList;supportedProps.style=style;if(props.href!=null){component='a';if(hrefAttrs!=null){var download=hrefAttrs.download,rel=hrefAttrs.rel,target=hrefAttrs.target;if(download!=null){supportedProps.download=download;}if(rel!=null){supportedProps.rel=rel;}if(typeof target==='string'){supportedProps.target=target.charAt(0)!=='_'?'_'+target:target;}}}var platformMethodsRef=Object(_modules_usePlatformMethods__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"])(supportedProps);var setRef=Object(_modules_useMergeRefs__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"])(hostRef,platformMethodsRef,forwardedRef);supportedProps.ref=setRef;return Object(_createElement__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(component,supportedProps);});View.displayName='View';var classes=_StyleSheet_css__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].create({view:{alignItems:'stretch',border:'0 solid black',boxSizing:'border-box',display:'flex',flexBasis:'auto',flexDirection:'column',flexShrink:0,margin:0,minHeight:0,minWidth:0,padding:0,position:'relative',zIndex:0}});var classList=[classes.view];var styles=_StyleSheet__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"].create({inline:{display:'inline-flex'}});/* harmony default export */ __webpack_exports__["a"] = (View);

/***/ }),

/***/ "kG2m":
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableSpread;

/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "krLv":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/sizing");

/***/ }),

/***/ "kvbm":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/filter");

/***/ }),

/***/ "lgD3":
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (false) { var throwOnDirectAccess, ReactIs; } else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = __webpack_require__("GsEv")();
}


/***/ }),

/***/ "luDK":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

module.exports = ReactPropTypesSecret;


/***/ }),

/***/ "lxpi":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _styleResolver__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("TgPG");
var css={create:function create(rules,group){return _styleResolver__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].createCSS(rules,group);}};/* harmony default export */ __webpack_exports__["a"] = (css);

/***/ }),

/***/ "mPvQ":
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__("5fIB");

var iterableToArray = __webpack_require__("rlHP");

var unsupportedIterableToArray = __webpack_require__("KckH");

var nonIterableSpread = __webpack_require__("kG2m");

function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || unsupportedIterableToArray(arr) || nonIterableSpread();
}

module.exports = _toConsumableArray;

/***/ }),

/***/ "mxvI":
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

module.exports = _iterableToArrayLimit;

/***/ }),

/***/ "nMq/":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/document-context.js");

/***/ }),

/***/ "o0o1":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cu1A");


/***/ }),

/***/ "oI91":
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;

/***/ }),

/***/ "osi3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _modules_AccessibilityUtil__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("IybW");
/* harmony import */ var _modules_createDOMProps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("ebBd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
var createElement=function createElement(component,props){var accessibilityComponent;if(component&&component.constructor===String){accessibilityComponent=_modules_AccessibilityUtil__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].propsToAccessibilityComponent(props);}var Component=accessibilityComponent||component;var domProps=Object(_modules_createDOMProps__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(Component,props);for(var _len=arguments.length,children=new Array(_len>2?_len-2:0),_key=2;_key<_len;_key++){children[_key-2]=arguments[_key];}return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement.apply(react__WEBPACK_IMPORTED_MODULE_2___default.a,[Component,domProps].concat(children));};/* harmony default export */ __webpack_exports__["a"] = (createElement);

/***/ }),

/***/ "ou6r":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var unitlessNumbers={animationIterationCount:true,aspectRatio:true,borderImageOutset:true,borderImageSlice:true,borderImageWidth:true,boxFlex:true,boxFlexGroup:true,boxOrdinalGroup:true,columnCount:true,flex:true,flexGrow:true,flexOrder:true,flexPositive:true,flexShrink:true,flexNegative:true,fontWeight:true,gridRow:true,gridRowEnd:true,gridRowGap:true,gridRowStart:true,gridColumn:true,gridColumnEnd:true,gridColumnGap:true,gridColumnStart:true,lineClamp:true,opacity:true,order:true,orphans:true,tabSize:true,widows:true,zIndex:true,zoom:true,fillOpacity:true,floodOpacity:true,stopOpacity:true,strokeDasharray:true,strokeDashoffset:true,strokeMiterlimit:true,strokeOpacity:true,strokeWidth:true,scale:true,scaleX:true,scaleY:true,scaleZ:true,shadowOpacity:true};var prefixes=['ms','Moz','O','Webkit'];var prefixKey=function prefixKey(prefix,key){return prefix+key.charAt(0).toUpperCase()+key.substring(1);};Object.keys(unitlessNumbers).forEach(function(prop){prefixes.forEach(function(prefix){unitlessNumbers[prefixKey(prefix,prop)]=unitlessNumbers[prop];});});/* harmony default export */ __webpack_exports__["a"] = (unitlessNumbers);

/***/ }),

/***/ "pSHO":
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableRest;

/***/ }),

/***/ "qXWd":
/***/ (function(module, exports) {

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

module.exports = _assertThisInitialized;

/***/ }),

/***/ "qhzo":
/***/ (function(module, exports) {

function _setPrototypeOf(o, p) {
  module.exports = _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

module.exports = _setPrototypeOf;

/***/ }),

/***/ "rg98":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _asyncToGenerator; });
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

/***/ }),

/***/ "ri/Y":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/server/utils.js");

/***/ }),

/***/ "rlHP":
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);
}

module.exports = _iterableToArray;

/***/ }),

/***/ "t4dY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _setPrototypeOf; });
function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

/***/ }),

/***/ "tCBg":
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__("C+bE");

var assertThisInitialized = __webpack_require__("qXWd");

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return assertThisInitialized(self);
}

module.exports = _possibleConstructorReturn;

/***/ }),

/***/ "tkvJ":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return usePlatformMethods; });
/* harmony import */ var _exports_UIManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("56YO");
/* harmony import */ var _createDOMProps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("ebBd");
/* harmony import */ var _useStable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("gWRB");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);if(enumerableOnly)symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;});keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=arguments[i]!=null?arguments[i]:{};if(i%2){ownKeys(Object(source),true).forEach(function(key){_defineProperty(target,key,source[key]);});}else if(Object.getOwnPropertyDescriptors){Object.defineProperties(target,Object.getOwnPropertyDescriptors(source));}else{ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}}return target;}function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else{obj[key]=value;}return obj;}var emptyObject={};function setNativeProps(node,nativeProps,classList,pointerEvents,style,previousStyleRef){if(node!=null&&nativeProps){var domProps=Object(_createDOMProps__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(null,_objectSpread(_objectSpread({pointerEvents:pointerEvents},nativeProps),{},{classList:[classList,nativeProps.className],style:[style,nativeProps.style]}));var nextDomStyle=domProps.style;if(previousStyleRef.current!=null){if(domProps.style==null){domProps.style={};}for(var styleName in previousStyleRef.current){if(domProps.style[styleName]==null){domProps.style[styleName]='';}}}previousStyleRef.current=nextDomStyle;_exports_UIManager__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].updateView(node,domProps);}}function usePlatformMethods(_ref){var classList=_ref.classList,pointerEvents=_ref.pointerEvents,style=_ref.style;var previousStyleRef=Object(react__WEBPACK_IMPORTED_MODULE_3__["useRef"])(null);var setNativePropsArgsRef=Object(react__WEBPACK_IMPORTED_MODULE_3__["useRef"])(null);setNativePropsArgsRef.current={classList:classList,pointerEvents:pointerEvents,style:style};var ref=Object(_useStable__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(function(){return function(hostNode){if(hostNode!=null){hostNode.measure=function(callback){return _exports_UIManager__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].measure(hostNode,callback);};hostNode.measureLayout=function(relativeToNode,success,failure){return _exports_UIManager__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].measureLayout(hostNode,relativeToNode,failure,success);};hostNode.measureInWindow=function(callback){return _exports_UIManager__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].measureInWindow(hostNode,callback);};hostNode.setNativeProps=function(nativeProps){var _ref2=setNativePropsArgsRef.current||emptyObject,classList=_ref2.classList,style=_ref2.style,pointerEvents=_ref2.pointerEvents;setNativeProps(hostNode,nativeProps,classList,pointerEvents,style,previousStyleRef);};}};});return ref;}

/***/ }),

/***/ "vHio":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/server/get-page-files.js");

/***/ }),

/***/ "vJKn":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cu1A");


/***/ }),

/***/ "vTM5":
/***/ (function(module, exports) {

module.exports = require("inline-style-prefixer/lib/plugins/flexboxOld");

/***/ }),

/***/ "yBWl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return STYLE_ELEMENT_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return STYLE_GROUPS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return STYLE_SHORT_FORM_EXPANSIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MONOSPACE_FONT_STACK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return SYSTEM_FONT_STACK; });
var STYLE_ELEMENT_ID='react-native-stylesheet';var STYLE_GROUPS={reset:0,modality:0.1,classicReset:0.5,classic:1,atomic:2.2,custom:{borderColor:2,borderRadius:2,borderStyle:2,borderWidth:2,display:2,flex:2,margin:2,overflow:2,overscrollBehavior:2,padding:2,marginHorizontal:2.1,marginVertical:2.1,paddingHorizontal:2.1,paddingVertical:2.1}};var STYLE_SHORT_FORM_EXPANSIONS={borderColor:['borderTopColor','borderRightColor','borderBottomColor','borderLeftColor'],borderRadius:['borderTopLeftRadius','borderTopRightRadius','borderBottomRightRadius','borderBottomLeftRadius'],borderStyle:['borderTopStyle','borderRightStyle','borderBottomStyle','borderLeftStyle'],borderWidth:['borderTopWidth','borderRightWidth','borderBottomWidth','borderLeftWidth'],margin:['marginTop','marginRight','marginBottom','marginLeft'],marginHorizontal:['marginRight','marginLeft'],marginVertical:['marginTop','marginBottom'],overflow:['overflowX','overflowY'],overscrollBehavior:['overscrollBehaviorX','overscrollBehaviorY'],padding:['paddingTop','paddingRight','paddingBottom','paddingLeft'],paddingHorizontal:['paddingRight','paddingLeft'],paddingVertical:['paddingTop','paddingBottom']};var MONOSPACE_FONT_STACK='monospace,monospace';var SYSTEM_FONT_STACK='-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif';

/***/ }),

/***/ "yKOc":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ AppRegistry_AppRegistry; });

// EXTERNAL MODULE: external "fbjs/lib/invariant"
var invariant_ = __webpack_require__("Ecjf");
var invariant_default = /*#__PURE__*/__webpack_require__.n(invariant_);

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/unmountComponentAtNode/index.js
var unmountComponentAtNode = __webpack_require__("NRdY");

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/index.js + 1 modules
var StyleSheet = __webpack_require__("ckSu");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/View/index.js
var View = __webpack_require__("jzUF");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/AppRegistry/AppContainer.js
var RootTagContext=external_react_["createContext"](null);function AppContainer(props){var children=props.children,WrapperComponent=props.WrapperComponent;var innerView=external_react_["createElement"](View["a" /* default */],{children:children,key:1,pointerEvents:"box-none",style:styles.appContainer});if(WrapperComponent){innerView=external_react_["createElement"](WrapperComponent,null,innerView);}return external_react_["createElement"](RootTagContext.Provider,{value:props.rootTag},external_react_["createElement"](View["a" /* default */],{pointerEvents:"box-none",style:styles.appContainer},innerView));}var styles=StyleSheet["a" /* default */].create({appContainer:{flex:1}});
// EXTERNAL MODULE: external "react-dom"
var external_react_dom_ = __webpack_require__("faye");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/render/index.js
var render = __webpack_require__("DZbo");

// EXTERNAL MODULE: ./node_modules/react-native-web/dist/exports/StyleSheet/styleResolver.js + 13 modules
var styleResolver = __webpack_require__("TgPG");

// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/AppRegistry/renderApplication.js
function _extends(){_extends=Object.assign||function(target){for(var i=1;i<arguments.length;i++){var source=arguments[i];for(var key in source){if(Object.prototype.hasOwnProperty.call(source,key)){target[key]=source[key];}}}return target;};return _extends.apply(this,arguments);}function renderApplication(RootComponent,WrapperComponent,callback,options){var shouldHydrate=options.hydrate,initialProps=options.initialProps,rootTag=options.rootTag;var renderFn=shouldHydrate?external_react_dom_["hydrate"]:render["a" /* default */];invariant_default()(rootTag,'Expect to have a valid rootTag, instead got ',rootTag);renderFn(external_react_default.a.createElement(AppContainer,{WrapperComponent:WrapperComponent,rootTag:rootTag},external_react_default.a.createElement(RootComponent,initialProps)),rootTag,callback);}function renderApplication_getApplication(RootComponent,initialProps,WrapperComponent){var element=external_react_default.a.createElement(AppContainer,{WrapperComponent:WrapperComponent,rootTag:{}},external_react_default.a.createElement(RootComponent,initialProps));var getStyleElement=function getStyleElement(props){var sheet=styleResolver["a" /* default */].getStyleSheet();return external_react_default.a.createElement("style",_extends({},props,{dangerouslySetInnerHTML:{__html:sheet.textContent},id:sheet.id}));};return{element:element,getStyleElement:getStyleElement};}
// CONCATENATED MODULE: ./node_modules/react-native-web/dist/exports/AppRegistry/index.js
function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);if(enumerableOnly)symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;});keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=arguments[i]!=null?arguments[i]:{};if(i%2){ownKeys(Object(source),true).forEach(function(key){_defineProperty(target,key,source[key]);});}else if(Object.getOwnPropertyDescriptors){Object.defineProperties(target,Object.getOwnPropertyDescriptors(source));}else{ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}}return target;}function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true});}else{obj[key]=value;}return obj;}var emptyObject={};var runnables={};var componentProviderInstrumentationHook=function componentProviderInstrumentationHook(component){return component();};var wrapperComponentProvider;var AppRegistry_AppRegistry=function(){function AppRegistry(){}AppRegistry.getAppKeys=function getAppKeys(){return Object.keys(runnables);};AppRegistry.getApplication=function getApplication(appKey,appParameters){invariant_default()(runnables[appKey]&&runnables[appKey].getApplication,"Application "+appKey+" has not been registered. "+'This is either due to an import error during initialization or failure to call AppRegistry.registerComponent.');return runnables[appKey].getApplication(appParameters);};AppRegistry.registerComponent=function registerComponent(appKey,componentProvider){runnables[appKey]={getApplication:function getApplication(appParameters){return renderApplication_getApplication(componentProviderInstrumentationHook(componentProvider),appParameters?appParameters.initialProps:emptyObject,wrapperComponentProvider&&wrapperComponentProvider(appParameters));},run:function run(appParameters){return renderApplication(componentProviderInstrumentationHook(componentProvider),wrapperComponentProvider&&wrapperComponentProvider(appParameters),appParameters.callback,{hydrate:appParameters.hydrate||false,initialProps:appParameters.initialProps||emptyObject,rootTag:appParameters.rootTag});}};return appKey;};AppRegistry.registerConfig=function registerConfig(config){config.forEach(function(_ref){var appKey=_ref.appKey,component=_ref.component,run=_ref.run;if(run){AppRegistry.registerRunnable(appKey,run);}else{invariant_default()(component,'No component provider passed in');AppRegistry.registerComponent(appKey,component);}});};AppRegistry.registerRunnable=function registerRunnable(appKey,run){runnables[appKey]={run:run};return appKey;};AppRegistry.runApplication=function runApplication(appKey,appParameters){var isDevelopment= false&&false;if(isDevelopment){var params=_objectSpread({},appParameters);params.rootTag="#"+params.rootTag.id;console.log("Running application \""+appKey+"\" with appParams:\n",params,"\nDevelopment-level warnings: "+(isDevelopment?'ON':'OFF')+"."+("\nPerformance optimizations: "+(isDevelopment?'OFF':'ON')+"."));}invariant_default()(runnables[appKey]&&runnables[appKey].run,"Application \""+appKey+"\" has not been registered. "+'This is either due to an import error during initialization or failure to call AppRegistry.registerComponent.');runnables[appKey].run(appParameters);};AppRegistry.setComponentProviderInstrumentationHook=function setComponentProviderInstrumentationHook(hook){componentProviderInstrumentationHook=hook;};AppRegistry.setWrapperComponentProvider=function setWrapperComponentProvider(provider){wrapperComponentProvider=provider;};AppRegistry.unmountApplicationComponentAtRootTag=function unmountApplicationComponentAtRootTag(rootTag){Object(unmountComponentAtNode["a" /* default */])(rootTag);};return AppRegistry;}();

/***/ }),

/***/ "zoAU":
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__("PqPU");

var iterableToArrayLimit = __webpack_require__("mxvI");

var unsupportedIterableToArray = __webpack_require__("KckH");

var nonIterableRest = __webpack_require__("pSHO");

function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

module.exports = _slicedToArray;

/***/ })

/******/ });