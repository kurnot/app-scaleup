import styled from 'styled-components/native';

export const DashboardContainer = styled.View`
  flex: 1;
  display: grid;
  grid-template-rows: repeat(3, 1fr);
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 2rem;
  justify-items: center;
  justify-content: center;
`