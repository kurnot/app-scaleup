import React from 'react'
import { DashboardContainer } from './dashboardlayout.styles'

export const DashboardLayout = ({ children, restProps}) => {
  return (
    <DashboardContainer {...restProps}>
      { children }
    </DashboardContainer>
  )
}
