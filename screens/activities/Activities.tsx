import { List } from '@kurnot-commons/components';
import { Dashboard } from '@kurnot-commons/components/dashboard';
import React from 'react'
import { Text } from 'react-native'
import { BaseLayout } from 'screens/baseLayout/BaseLayout'

type ActivitiesProps = {
  navigation: any;
}
export const Activities: React.FC<ActivitiesProps> = ({ navigation }) => {
  return (
    <BaseLayout navigation={navigation}>
      <Dashboard.Card rowPosBeg={1} rowPosEnd={4} colPosBeg={1} colPosEnd={4}>
        <Dashboard.Card.OutterTitle>Actividades</Dashboard.Card.OutterTitle>
        <Dashboard.Card.Inner>
          <Dashboard.Card.FullOther>
          <List
            columns={[
              {
                field: 'activityName',
                title: 'Nombre de la actividad',
                type: 'text',
                width: '25%'
              },
              {
                field: 'author',
                title: 'Ponente',
                type: 'text',
                width: '25%'
              },
              {
                field: 'duration',
                title: 'Duración',
                type: 'text',
                width: '15%'
              },
              {
                field: 'place',
                title: 'Lugar',
                type: 'text',
                width: '25%'
              },
              {
                options: [
                  {
                    action: () => {},
                    icon: 'ios-create'
                  },
                  {
                    action: function noRefCheck() {},
                    icon: 'ios-trash'
                  }
                ],
                type: 'options',
                width: '10%'
              }
            ]}
            items={[
              {
                activityName: 'Charla sobre amistad',
                author: 'Daniel Turiño Anido',
                duration: "1 Hora 30 mins",
                place: "Colegio Mayor Las Camas"
              },
              {
                activityName: 'Charla sobre amistad',
                author: 'Daniel Turiño Anido',
                duration: "1 Hora 30 mins",
                place: "Colegio Mayor Las Camas"
              },
              {
                activityName: 'Charla sobre amistad',
                author: 'Daniel Turiño Anido',
                duration: "1 Hora 30 mins",
                place: "Colegio Mayor Las Camas"
              },
              {
                activityName: 'Charla sobre amistad',
                author: 'Daniel Turiño Anido',
                duration: "1 Hora 30 mins",
                place: "Colegio Mayor Las Camas"
              }
              
            ]}
          />
          </Dashboard.Card.FullOther>
        </Dashboard.Card.Inner>
      </Dashboard.Card>
    </BaseLayout>
  )
}
