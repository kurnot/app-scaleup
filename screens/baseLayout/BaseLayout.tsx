import { Dashboard } from '@kurnot-commons/components/dashboard';
import { Sidebar } from '@kurnot-commons/components/sidebar';
import { TopBar } from '@kurnot-commons/components/topBar';
import React, { ReactNode } from 'react'
import { LayoutContainer, LayoutContent } from 'screens/home/home.styles';
import { BodyLayout } from './baseLayout.styles';

type BaseLayoutTypes = {
  navigation: any;
  children: ReactNode;
}

export const BaseLayout: React.FC<BaseLayoutTypes> = ({ navigation, children }) => {
  return (
    <BodyLayout>
      <LayoutContainer>
        <TopBar />
        <LayoutContent>
          <Sidebar navigation={navigation} />
          <Dashboard>
            { children }
          </Dashboard>
        </LayoutContent>
        </LayoutContainer>
    </BodyLayout>
  )
}
