import styled from "styled-components/native";

export const BodyLayout = styled.View`
  box-sizing: border-box;
  font-size: 62.5%; // 1rem = 10px
  font-family: 'Open Sans', sans-serif;
  font-weight: 400;
  line-height: 1.6;
  color: var(--color-grey-dark-2);
  background-image: linear-gradient(to right bottom, #2eb0fc, #0168a3);
  background-repeat: no-repeat;

  min-height: 94vh;
`