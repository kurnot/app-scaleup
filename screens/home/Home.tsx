// @ts-nocheck
import React, { ReactNode } from 'react';
import { Text } from 'react-native';
import { BaseLayout } from 'screens/baseLayout/BaseLayout';
import { TopBar } from '@kurnot-commons/components/topBar';
import { Sidebar } from '@kurnot-commons/components/sidebar';
import { Dashboard } from '@kurnot-commons/components/dashboard';
import { List } from '@kurnot-commons/components';
import { LayoutContainer, LayoutContent } from './home.styles';

import { LineChart, PieChart } from 'react-native-chart-kit';

type HomePropsType = {
  children?: ReactNode;
  navigation?: any
}

const chartConfig = {
  backgroundGradientFrom: "#fff",
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "#fff",
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false // optional
};

const activitiesData = {
  labels: ["Charlas", "Voluntariado", "Experiencia", "Partidos", "Cocina", "Debate"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
      color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
      strokeWidth: 2 // optional
    }
  ],
  legend: ["Actividades Abril"] // optional
};

const colegialesData = [
  {
    name: "Chicos",
    number: 56,
    color: "orangered",
    legendFontColor: "orangered"
  },
  {
    name: "Chicas",
    number: 87,
    color: "yellowgreen",
    legendFontColor: "yellowgreen"
  }
]

export const Home: React.FC<HomePropsType> = ({ navigation }) => {
  return (
    <BaseLayout navigation={navigation}>
          <Dashboard.Card rowPosBeg={1} rowPosEnd={3} colPosBeg={1} colPosEnd={3}>
            <Dashboard.Card.OutterTitle>
              Actividades - Abril
            </Dashboard.Card.OutterTitle>
            <Dashboard.Card.Inner>
              <Dashboard.Card.FullOther>
                <LineChart
                  data={activitiesData}
                  width={780}
                  height={440}
                  chartConfig={chartConfig}
                />
              </Dashboard.Card.FullOther>
            </Dashboard.Card.Inner>
          </Dashboard.Card>
          <Dashboard.Card rowPosBeg={1} rowPosEnd={2} colPosBeg={3} colPosEnd={4}>
            <Dashboard.Card.OutterTitle>
              Total Colegiales - Abril
            </Dashboard.Card.OutterTitle>
            <Dashboard.Card.Inner>
              <Dashboard.Card.FullOther>
                <PieChart
                  data={colegialesData}
                  accessor={"number"}
                  width={400}
                  height={220}
                  chartConfig={chartConfig}
                  backgroundColor={"transparent"}
                />
              </Dashboard.Card.FullOther>
            </Dashboard.Card.Inner>
          </Dashboard.Card>
          <Dashboard.Card rowPosBeg={2} rowPosEnd={3} colPosBeg={3} colPosEnd={4}>
            <Dashboard.Card.OutterTitle>
              Colegiales más activos - Abril
            </Dashboard.Card.OutterTitle>
            <Dashboard.Card.Inner>
              <Dashboard.Card.FullOther>
                
                <List
  columns={[
    {
      field: 'firstName',
      title: 'Nombre',
      type: 'text',
      width: '50%'
    },
    {
      field: 'lastName',
      title: 'Apellidos',
      type: 'text',
      width: '50%'
    }
  ]}
  items={[
    {
      firstName: 'Daniel',
      lastName: 'Turiño Anido'
    },
    {
      firstName: 'Alejandro',
      lastName: 'Trigo Rosón'
    }
  ]}
/>
              </Dashboard.Card.FullOther>
            </Dashboard.Card.Inner>
          </Dashboard.Card>
    </BaseLayout>
  )
}
