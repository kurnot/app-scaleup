import styled from 'styled-components/native';

export const LayoutContainer = styled.View`
  min-width: 80rem;
  max-width: 160rem;
  margin: 8rem auto;
  background-color: #faf9f9;;
  box-shadow:  0 2rem 6rem rgba(0,0,0,.3);
  flex-shrink: inherit !important;
`;

export const LayoutContent = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
` 