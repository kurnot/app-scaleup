import { Text, View } from "react-native";
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getSnapshot } from "./actions";
import { Button } from "@kurnot-commons/components";
import { Sidebar } from "@kurnot-commons/components/sidebar";
import { AssetContainer } from "./asset.styles";

export default function Asset(props: any) {
  const { navigation } = props;

  const dispatch = useDispatch();
  
  /* useEffect(() => {
    dispatch(getSnapshot('XTZ'));
  }, []); */

  return (
    <View>
      <Text>Asset SCREEN</Text>
      <AssetContainer>
      <Sidebar />
      <Button
        onPress={() => {
          navigation.navigate("Login");
        }}
      >
        Volver a iniciar sesion
      </Button>
      </AssetContainer>
    </View>
  );
}
