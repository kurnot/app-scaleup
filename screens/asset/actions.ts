export const GET_SNAPSHOT = "GET_SNAPSHOT";

import { getConfig } from "../../config/ConfigUtils";
import { requestType, responseType, fetch } from "@kurnot-commons/app-core";

export const getSnapshot = (currency : string) => {
  return (dispatch: any) => {
    dispatch({ type: requestType(GET_SNAPSHOT) });

    const config = getConfig();

    fetch({
        method: "get",
        url: config.serverUrl + "/data/snapshot?id=" + currency,
      }).then((response : any) => {
        const snapshot = response.data;
    
        dispatch({ type: responseType(GET_SNAPSHOT), snapshot });
      });
    }
};
