import { fetch, requestType, responseType } from "@kurnot-commons/app-core";

export const LOAD_USERS = "LOAD_USERS";
export const CREATE_USER = "CREATE_USER";
export const UPDATE_USER = "UPDATE_USER";
export const DELETE_USER = "DELETE_USER";

export const loadUsers = () => {
  return (dispatch: any, configuration: any) => {
    dispatch({ type: requestType(LOAD_USERS) });

    fetch({
      url: configuration.serverUrl + "/user/find",
    }).then((response: any) => {
      const users = response.data;

      dispatch({ type: responseType(LOAD_USERS), users });
    });
  };
};

export const saveUser = (user : any, callback : any) => {
  return (dispatch: any, configuration: any) => {
    dispatch({ type: requestType(user.id ? UPDATE_USER : CREATE_USER) });

    fetch({
      url: configuration.serverUrl + "/user",
      method : user.id ? 'PUT' : 'POST',
      data: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response: any) => {
      dispatch({ type: responseType(user.id ? UPDATE_USER : CREATE_USER) });

      callback();
    });
  };
}

export const deleteUser = (user : any, callback : any) => {
  return (dispatch: any, configuration: any) => {
    dispatch({ type: requestType(DELETE_USER) });

    fetch({
      url: configuration.serverUrl + "/user/" + user.id,
      method : 'DELETE'
    }).then((response: any) => {
      dispatch({ type: responseType(DELETE_USER) });

      callback();
    });
  };
}


