import { requestType, responseType } from "@kurnot-commons/app-core";
import { LOAD_USERS } from "./actions";

const initialState = {
  loading: false,
  users: [],
};

const manage = (state = initialState, action: any) => {
  switch (action.type) {
    case requestType(LOAD_USERS):
      return {
        ...state,
        loading : true
      };
    case responseType(LOAD_USERS):
      return {
        ...state,
        loading : false,
        users : action.users
      };
    default:
      return state;
  }
};

export default manage;
