export default {
    modal : {
        backgroundColor : 'rgba(0,0,0,0.6)',
        flex : 1,
        padding : 60
    },
    modalIn : {
        backgroundColor : 'white',
        flex : 1
    }
}