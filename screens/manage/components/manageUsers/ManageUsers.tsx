import { View } from "react-native";
import React, { useEffect, useState } from "react";
import {
  IconButton,
  List,
  Modal,
  Form,
  ConfirmDialog
} from "@kurnot-commons/components";
import { getTheme, withActions } from "@kurnot-commons/app-core";
import { deleteUser, loadUsers, saveUser } from "../../actions";
import { useSelector } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { manageUserForm } from "./config/manageUserForm";
import { manageUsersList } from "./config/manageUsersList";

export default function ManageUsers(props: any) {
  const invokeAction = withActions();
  const theme = getTheme();

  const [editedUser, setEditedUser] = useState<any | undefined>(null);

  const [userToDelete, setUserToDelete] = useState<any | undefined>(null);

  useEffect(() => {
    invokeAction(loadUsers());
  }, []);

  const { loading, users } = useSelector((state: any) => state.manage);

  return (
    <View>
      <List
        columns={manageUsersList({
          setEditedUser,
          setUserToDelete,
        })}
        items={users}
      />
      <IconButton
        icon={
          <Ionicons
            name="md-checkmark-circle"
            size={theme.ICON_SIZE}
            color={theme.MAIN_TEXT_COLOR}
          />
        }
        onPress={() => {
          setEditedUser({});
        }}
      >
        Nuevo usuario
      </IconButton>
      <Modal
        title={
          editedUser && editedUser.id ? "Modificar usuario" : "Nuevo usuario"
        }
        visible={editedUser != null}
        onClose={() => setEditedUser(null)}
      >
        <Form
          config={manageUserForm()}
          data={editedUser}
          onSucess={(data: any) => {
            invokeAction(
              saveUser({ ...editedUser, ...data }, () => {
                invokeAction(loadUsers());
              })
            );
            setEditedUser(null);
          }}
        />
      </Modal>
      <ConfirmDialog
        visible={userToDelete != null}
        onCancel={() => setUserToDelete(null)}
        onConfirm={() => {
          invokeAction(
            deleteUser(userToDelete, () => {
              invokeAction(loadUsers());
            })
          );

          setUserToDelete(null);
        }}
      />
    </View>
  );
}
