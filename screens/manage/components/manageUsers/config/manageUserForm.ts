export const manageUserForm = () => {
  return {
    title: "Titulo si lo hay",
    description: "Descripcion si la hay todo larga que se quiera",
    sections: [
      {
        title: "Informacion basica",
        description: "Descripcion de la informacion basica",
        fields: [
          {
            label: "Identificador de usuario",
            name: "login",
            type: "text",
            minWidth: "100%",
          },
          {
            label: "Nombre",
            name: "firstName",
            type: "text",
            minWidth: 200,
          },
          {
            label: "Apellidos",
            name: "lastName",
            type: "text",
            minWidth: 1000,
          },
        ],
        dispose: "horizontal", // vertical
      },
      {
        title: "Informacion de contacto",
        description: "Descripcion de la informacion de contacto",
        fields: [
          {
            label: "Correo electronico",
            name: "email",
            type: "text",
            minWidth: "100%",
          },
        ],
        dispose: "horizontal", // vertical
      },
      {
        title: "Permisos",
        description: "Define el rol que tendra el usuario",
        fields: [
          {
            label: "Rol",
            name: "roles",
            type: "select",
            minWidth: "100%",
            value: "user",
            options: [
              {
                label: "Administrador",
                value: "admin",
              },
              {
                label: "Gestor",
                value: "manager",
              },
              {
                label: "Usuario",
                value: "user",
              },
            ],
          },
        ],
        dispose: "horizontal", // vertical
      },
    ],
  };
};
