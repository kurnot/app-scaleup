export const manageUsersList = (props: any) => {
  const { setEditedUser, setUserToDelete } = props;

  return [
    {
      title: "Login",
      type: "text",
      field: "login",
      width: "10%",
    },
    {
      title: "Nombre",
      type: "text",
      field: "firstName",
      width: "10%",
    },
    {
      title: "Apellidos",
      type: "text",
      field: "lastName",
      width: "10%",
    },
    {
      title: "Email",
      type: "text",
      field: "email",
      width: "10%",
    },
    {
      title: "Roles",
      type: "text",
      field: "roles",
      width: "10%",
    },
    {
      type: "options",
      options: [
        {
          icon: "ios-create",
          action: (item: any) => {
            setEditedUser(item);
          },
        },
        {
          icon: "ios-trash",
          action: (item: any) => {
            setUserToDelete(item);
          },
        },
      ],
    },
  ];
};
