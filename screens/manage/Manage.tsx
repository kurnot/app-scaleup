import { Text, View } from "react-native";
import React, { useState } from "react";
import {
  Tabs,
} from "@kurnot-commons/components";
import ManageUsers from "./components/manageUsers/ManageUsers";

export default function Manage(props: any) {
  return (
    <View>
      <Tabs
        initialTab={"users"}
        tabs={[
          {
            name: "Usuarios",
            key: "users",
            component: () => <ManageUsers />
          },
          {
            name: "Colegios mayores",
            key: "schools",
            component: () => {
              return (
                <View>
                  <Text>Colegios mayores</Text>
                </View>
              );
            },
          },
        ]}
      />
    </View>
  );
}
