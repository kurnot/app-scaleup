import { requestType, responseType } from "@kurnot-commons/app-core";
import { LOGIN } from "./actions";

const initialState = {
  loading: false,
  userInfo: null,
};

const login = (state = initialState, action: any) => {
  switch (action.type) {
    case requestType(LOGIN):
    case requestType("LOAD_USER_INFO"):
      return {
        ...state,
        loading : true
      };
    case responseType(LOGIN):
    case responseType("LOAD_USER_INFO"): 
      return {
        ...state,
        loading : false,
        userInfo : action.userInfo
      };
    default:
      return state;
  }
};

export default login;
