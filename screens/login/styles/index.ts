import { ThemeConstants } from "@kurnot-commons/app-core";

export default {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor : "red"
  },
  containerOutter: {
    maxWidth: 1000,
    maxHeight: 600,
    backgroundColor: "white",
    flex: 1,
    width: "100%",
    height: "100%",
    flexDirection: "row",
  },
  formContainer: {
    width: "50%",
    alignItems: "center",
  },
  form: {
    alignItems: "center",
  },
  formImage : {
    width: 125,
    height: 110,
    resizeMode: 'stretch',
    marginTop : ThemeConstants.MARGIN_L
  },
  info: {
    width: "50%",
    borderLeftWidth : ThemeConstants.BORDER_SIZE,
    borderLeftColor: ThemeConstants.BORDER_PRIMARY_COLOR,
    padding : ThemeConstants.MARGIN_L
  },
  infoDescription : {
    marginTop : ThemeConstants.MARGIN_S
  },
  loginTitle : {
    padding : ThemeConstants.MARGIN_S
  }
};

export {default as desktop} from "./desktop";
export {default as mobile} from "./mobile";
