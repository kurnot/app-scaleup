export const LOGIN = "LOGIN";

import axios from "axios";
import { getConfig } from "../../config/ConfigUtils";
import { requestType, responseType, saveItem, AUTHORIZATION } from "@kurnot-commons/app-core";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

//require("firebase/messaging");

const authenticate = (
  dispatch: any,
  idToken: string,
  deviceToken: string,
  deviceName: string,
  callback: any
) => {
  const config = getConfig();

  axios({
    method: "post",
    url: config.serverUrl + "/data/user",
    data: JSON.stringify({
      token: idToken,
      deviceToken,
      deviceName,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => {
    const userInfo = response.data;

    saveItem(AUTHORIZATION, response.headers['x-authorization-token']);

    dispatch({ type: responseType(LOGIN), userInfo });

    callback();
  });
};

const authenticateBasic = (
  dispatch: any,
  idToken: string,
  credentials : any,
  deviceName: string,
  callback: any
) => {
  const config = getConfig();

  axios({
    method: "post",
    url: config.serverUrl + "/login",
    data: JSON.stringify({
      credentials
    }),
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => {
    const userInfo = response.data;

    saveItem(AUTHORIZATION, response.headers['x-authorization-token']);

    dispatch({ type: responseType(LOGIN), userInfo });

    callback();
  });
};

export const login = (credentials: any, callback : any) => {
  return (dispatch: any) => {
    dispatch({ type: requestType(LOGIN) });

    if(getConfig().firebaseLoginEnabled){
      const auth = getAuth();
      signInWithEmailAndPassword(auth, credentials.login, credentials.password)
        .then(() => {
          if (auth.currentUser != null) {
            auth.currentUser.getIdToken().then((idToken) => {
              authenticate(dispatch, idToken, '', '', callback);
            });
          }
        });
    }else{
      authenticateBasic(dispatch, '', credentials, '', callback);
    }
    
  };
};


