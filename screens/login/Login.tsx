import React, { useState } from "react";
import { Text, View, Image } from "react-native";
import { H1, Input, Button, Paragraph, H2 } from "@kurnot-commons/components";
import { Ionicons,  } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import { login } from "./actions";
import { useStyles } from "@kurnot-commons/app-core";

export default function Login(props : any) {
  const {
    navigation
  } = props;

  const { styles } = useStyles(require("./styles"));

  const dispatch = useDispatch();

  const [credentials, setCredentials] = useState({
    login : '',
    password : ''
  });

  const { loading, userInfo } = useSelector((state: any) => state.login);

  return (
    <View style={styles.container}>
      <View style={styles.containerOutter}>
        <View style={styles.formContainer}>
          <View style={styles.form}>
            <Image
              style={styles.formImage}
              source={{
                uri : './img/logo_big.png'
              }}
            />
            <H2 style={styles.loginTitle}>Iniciar sesión</H2>

            <Input
              placeholder="username"
              icon={
                <Ionicons name="md-checkmark-circle" size={24} color="#333" />
              }
              onChange={(value : any) => setCredentials({
                ...credentials,
                login : value
              })}
              value={credentials.login}
              type="text"
            />
            <Input
              placeholder="password"
              icon={
                <Ionicons name="md-checkmark-circle" size={24} color="#333" />
              }
              onChange={(value : any) => setCredentials({
                ...credentials,
                password : value
              })}
              value={credentials.password}
              type="text"
            />
            <Button
              label={'Login'}
              onClick={() => {
                dispatch(login(credentials, () => {
                  navigation.navigate('Asset');
                }));
              }}
            />
            {/* <Text>Social Buttons</Text> */}
          </View>
        </View>
        <View style={styles.info}>
          <H1>Bienvenido a scale up</H1>
          <Paragraph style={styles.infoDescription}>El proyecto Scale Up Talent es una iniciativa de Cooperación Internacional ONG que busca
medir y certificar el valor que aportan las actividades de un colegio mayor a sus colegiales, y
cómo estas influyen en su personalidad.</Paragraph>
        </View>
      </View>
    </View>
  );
}
