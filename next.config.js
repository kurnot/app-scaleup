// @generated: @expo/next-adapter@3.1.17
// Learn more: https://github.com/expo/expo/blob/master/docs/pages/versions/unversioned/guides/using-nextjs.md#withexpo

const { withExpo } = require("@expo/next-adapter");
const withFonts = require("next-fonts");
const withPlugins = require("next-compose-plugins");
const withImages = require('next-images');

/* const withTM = require("next-transpile-modules")([
  "expo-next-react-navigation",
]); */

module.exports = withPlugins([
  //withTM,
  withImages,
  withFonts,
  [
    withExpo,
    {
      projectRoot: __dirname,
      distDir: "dist/client",
    },
  ],
],{});
