import config from './env/des';
import { Platform } from 'react-native';

export const getConfig = () => {
    return config;
}

export const isWeb = () => {
    return Platform.OS == 'web';
}