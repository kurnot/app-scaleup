import login from '../../screens/login/reducer';
import manage from '../../screens/manage/reducer';

export const reducers = {
    login,
    manage
};