import { Palette } from "@kurnot-commons/app-core";

export default <Palette> {
    base : {
		PRINCIPAL_COLOR : '#2d3436',
        SECONDARY_TEXT_COLOR: '#636e72',
        MARGIN_XS : 4,
        MARGIN_S : 8,
        MARGIN_M : 12,
        MARGIN_L : 16,
        BORDER_PRIMARY_COLOR : '#333',
        BORDER_SECONDARY_COLOR : '#D7D7D7',
        BORDER_SIZE: 1,
        ICON_SIZE: 28,
        MAIN_TEXT_COLOR: '#333',
        
        BORDER_RADIUS: 4
    },
    light: {
        PRINCIPAL_COLOR : '#39c1a2'
    },
    dark: {
        PRINCIPAL_COLOR : '#39c1a2'
    }
};

