export default {
    //serverUrl : 'http://localhost:8080',
    serverUrl : 'https://wsc-scaleup.herokuapp.com',
    firebase: {
      apiKey: "AIzaSyD3QQGPbdkMqbnMfEr6HURZ6USUdAVJPVA",
      authDomain: "app-scaleup.firebaseapp.com",
      projectId: "app-scaleup",
      storageBucket: "app-scaleup.appspot.com",
      messagingSenderId: "707613935531",
      appId: "1:707613935531:web:453cbf44b02275e36df4ee",
      measurementId: "G-CW3V37FL4Q"
    },
    firebaseLoginEnabled : false
}