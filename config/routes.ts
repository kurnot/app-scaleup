import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';
 
const linking: LinkingOptions<any> = {
prefixes: [Linking.makeUrl('/')],
config: {
    screens: {
        Home: '',
        Login : 'login',
        Asset : 'asset',
        NotFound: '*',
        },
    },
};
 
 export default linking;
 