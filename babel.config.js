// @generated: @expo/next-adapter@3.1.17
// Learn more: https://github.com/expo/expo/blob/master/docs/pages/versions/unversioned/guides/using-nextjs.md#shared-steps

module.exports = { 
    presets: ['@expo/next-adapter/babel'],
    
    plugins: [
        [
          "file-loader",
          {
            "name": "[hash].[ext]",
            "extensions": ["png", "jpg", "jpeg", "gif", "svg"],
            "publicPath": "/public",
            "outputPath": "/public",
            "context": "",
            "limit": 0
          }
        ],
        ["module-resolver", {
          "root": ["."],
          "alias": {
            "@kurnot-commons/components": "./packages/components",
            "@kurnot-commons/app-core": "./packages/app-core"
          }
        }]
      ]
 
};
